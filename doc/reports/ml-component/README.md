# Project ML Component

# Description
> Subproject responsible for managing models services for inference, training and testing, as well as 
orchestrate their respectives workflows.

# Team
* **Fernando Filho**
  * Tabular model implementation and conversion to .onnx and plugin FASTApi enpoints code, plugin architecture definition.
* **Guilherme Corrêa**
  * Pipeline and plugin architecture definition, standardization of Tabular and Image plugins code, Image model implementation and conversion to .onnx.
* **Guilherme Pascon**
  * Architecture definition, project documentation and updation of current model in production plugin.
* **Henrique Mineto**
  * Architecture definition and project documentation, Image model inference service, Training step and data filtering in ML Pipeline.
* **João Hergert**
  * Pre-trained models research, test trainable ml models, documentation.
* **Luís Fernando**
  * Evaluate development, dockerfile, API definition.
* **Pietro Pieri**
  * Architecture definition, documentation, Tabular plugin code, Utils codes plugins, Tabular API implementation and Data Models definiton.

# GitLab Subproject
* https://gitlab.com/open-dish/datasci

# Folder and Files Structure

~~~
├── APIs Models Services
    ├── ImagePlugin
        ├── docker                  <- docker compose and bash scripts for building and running the container
        ├── model_config            <- model class, model config, pre trained weights and other files for running the model
        ├── utils                   <- arguments and auxiliary functions
        ├── Dockerfile              <- docker image definition
        ├── blood_cells_server.py   <- FastAPI server file
        └── requirements.txt
    └── TabularPlugin
        ├── docker                  <- docker compose and bash scripts for building and running the container
        ├── model_config            <- model class, model config, pre trained weights and other files for running the model
        ├── utils                   <- arguments and auxiliary functions
        ├── Dockerfile              <- docker image definition
        ├── blood_cells_server.py   <- FastAPI server file
        └── requirements.txt
├── Doc Requests
│
└── ML Pipeline
    ├── configuration
        ├── configuration.json       <- configuration related to integrated models 
        ├── configuration.py         <- object for configuration.json file
        └── model_configuration.py   <- object for model configuration file
    ├── domain     
        ├── data_schema.py           <- object representation for data object in model configuration
        ├── loinc_codes.json         <- LOINC codes used in pipeline
        ├── snomed_codes.json        <- SNOMED codes used in pipeline
        └── statistics.py            <- object representation for a model statistics
    ├── data
        ├── response                 <- object representations for Patient and Diagnostics data
        └── tabular_dataset.py       <- responsible for generating train and test datasets
    ├── evaluation
        └── evaluate.py              <- evaluates test metrics and updates production model
    ├── routers                      <- implementation of all exposed REST API endpoints used
        └── update-model-script.py   <- tests the functionallity of updating the current production model for a new one
    ├── train                        <- codes responsibles for orchestrating train and test steps in models
    └── utils                        <- centralized codes that were used to ease the whole implementation
~~~

# General Architecture or Process

## Plugin Architecture
> The given image illustrates what a plugin creator must implement so it can be used in our architecture. Every plugin must provide a .json config file and a docker image that opens a local API for training, testing, predicting and updating the prediction model.
![MLPluginArchitecture](./images/MLPluginSpecifications.jpeg)


## ML Pipeline Architecture
> In given image, vertical arrows represents regular calls between internal pipeline components. Meanwhile, horizontal arrows represents HTTP requests to different components.
![MLPipelineArchitecture](./images/MLPipelineArchitecture.png)

# Installation and Execution

All our code was made to run inside docker containers, so the instalation and execution of it is very simple.

## Plugins

Both plugins containers open a local server with the necessary routes but in different ports. Image plugin runs on port 4009 and tabular plugin runs on port 4011. 

There are already bash scripts for building and running the docker images, thus one can simply do:

~~~shell
cd <plugin-folder>/docker/
bash build.sh && bash run.sh
~~~

## ML Pipeline

ML Pipeline container opens a local server with the necessary routes running on port 4010. in order to build and run the docker image, is necessary to execute:

~~~shell
cd datasci/ML\ Pipeline/
docker-compose -f compose.yml build && docker-compose -f compose.yml up
~~~

# Data Models and APIs

> This section contains the details of the data models that ML components use.

## Tabular Data Models

![TabularDataModel](./images/TabularDataModel.png)

### Tabular Data Model Example

~~~json
{
   "patient":{
      "id":"4b8ae268-a30f-76a4-c560-66965dc7dc4f",
      "birthDate":"1970-06-26",
      "deathDate":"nan",
      "first":"Juliane",
      "last":"Ledner",
      "race":"white",
      "ethnicity":"nonhispanic",
      "gender":"M",
      "birthPlace":"Norwood  Massachusetts  US",
      "lat":"42.56172224791635",
      "lon":"-71.08529432514787"
   },
   "diagnostic":{
      "patient":"4b8ae268-a30f-76a4-c560-66965dc7dc4f",
      "encounter":"2463ab8c-120f-cd69-0480-8f785199a26e",
      "result":{
         "observation":[
            {
               "category":"survey",
               "code":"718-7",
               "value":"9.0",
               "units":"nan",
               "type":"text"
            },
            {
               "category":"survey",
               "code":"785-6",
               "value":"21.5",
               "units":"{score}",
               "type":"numeric"
            },
            {
               "category":"survey",
               "code":"786-4",
               "value":"29.6",
               "units":"nan",
               "type":"text"
            },
            {
               "category":"survey",
               "code":"787-2",
               "value":"71.2",
               "units":"{score}",
               "type":"numeric"
            }
         ]
      },
      "resultsInterpreter":"d280c09b-eec6-3c56-bdfe-8ae98404e4e6",
      "status":"final",
      "id":"589291b1-7e3d-9158-283d-015c7b0f445f",
   }
}
~~~

## Image Data Models

![ImageDataModel](./images/ImageDataModel.jpg)

### Image Data Model Example
~~~json
{
  "image": {
    "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
    "date":"2003-11-01T20:30:34Z",
    "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
    "encounter": "13efcb79-c259-e4a5-2258-ea655 ceeeb48",
    "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png"
  }
}
~~~

> All ML Puglins that use Tabular Data our Image Data in their pipelines will receive Data Structures similar the examples above. To check details of the Data Structures that we have in this project you can check this link: [Data Management API and Models](../../api/management.md).

## Plugins Output Datamodel
> It was implemented ML plugins for tabular classification and image object detection

### Image Object Detection
> The output data of image object detection models are bounding boxes, thus the data model is:

~~~json
{
  "output":[
    {
      "label": <string>,
      "confidence": <float>,
      "topleft":{
        "x":<int>,
        "y":<int>
      },
      "bottomright":{
        "x":<int>,
        "y":<int>
      }
    },
    ...
  ]
}
~~~

**Image Object Detection - Example**

~~~json
{
  "output":[
    {
      "label": "Platelets,
      "confidence": 0.854878,
      "topleft":{
        "x":0,
        "y":0
      },
      "bottomright":{
        "x":21,
        "y":24
      }
    },
    {
      "label": "RBC,
      "confidence": 0.564878,
      "topleft":{
        "x":5,
        "y":7
      },
      "bottomright":{
        "x":14,
        "y":21
      }
    }
  ]
}
~~~

**Tabula Plugin Object - Example**

~~~json
{
   "patient":"4b8ae268-a30f-76a4-c560-66965dc7dc4f",
   "encounter":"2463ab8c-120f-cd69-0480-8f785199a26e",
   "result":{
      "observation":[
         {
            "category":"survey",
            "code":"718-7",
            "value":"9.0",
            "units":"nan",
            "type":"text"
         },
         {
            "category":"survey",
            "code":"785-6",
            "value":"21.5",
            "units":"{score}",
            "type":"numeric"
         },
         {
            "category":"survey",
            "code":"786-4",
            "value":"29.6",
            "units":"nan",
            "type":"text"
         },
         {
            "category":"survey",
            "code":"787-2",
            "value":"71.2",
            "units":"{score}",
            "type":"numeric"
         }
      ]
   },
   "conclusionCode":[
      "271737000"
   ],
   "resultsInterpreter":"d280c09b-eec6-3c56-bdfe-8ae98404e4e6",
   "status":"final",
   "id":"589291b1-7e3d-9158-283d-015c7b0f445f",
   "study":[
      ""
   ],
   "reliability":[
      "99%"
   ]
}
~~~

## ML Pipeline Data Models

### Configuration Data Model
> Model guaranteed by each plugin with its own needed properties in order to ML Pipeline to function.
~~~json
{
  "model-name": <string>,
  "hyperparameters": <dict>,
  "pre-trained": <boolean>,
  "data-type": <string>,
  "policy": <string>,
  "data": {
    "observation": {
      "laboratory-results": [
        <string>,
        <string>,
        ...
      ],
      "vital-signs": [
        <string>,
        <string>,
        ...
      ]
    },
    "condition": [
      <string>,
      <string>,
      ...
    ],
    "patient": [
      <string>,
      <string>,
      ...
    ]
  },
  "triggers": [
    {
      "type": <string>,
      "value": <number>,
      "fields": [
        <string>,
        <string>,
        ...
      ]
    }
  ]
}
~~~ 

With the following schema details:
| Attribute  | Type | Required | Description  |
| :--------- | :------- | :------- | :----------- |
| `model-name` | string | Yes      | Name of `.onnx` file used by the plugin. |
| `hyperparameters` | Dictionary | No     | Default values of hyperparameters used in model training.            |
| `pre-trained` | boolean | Yes      | Indicates wheter the ML Model already has weights to be used for training.            |
| `data-type` | string | Yes      | Defines type of data input, for now we support `image` and `tabular`. |
| `policy` | string | Yes      | Policy by which the model is to be considered better than others. |
| `data` | object | Yes      | Set of domain data that represents researchable information based in [Data Management API and Models](../../api/management.md). |
| `data.observation` | object | No      | Data that can be filtered under observation fields, as defined in FHIR. |
| `data.observation.laboratory-results` | array of string | No    | List of laboratory results terms that have a LOINC code associated. |
| `data.observation.vital-signs` | array of string | No    | List of vital signs terms that have a LOINC code associated. |
| `data.condition` | array of string | No    | List of condition terms used as label for training step that have a SNOMED code associated. |
| `data.patient` | array of string | No    | List of patient-related information. |
| `triggers` | array of object | Yes      | Mecanism to activate training step in pipeline.            |
| `triggers[].type`    | string   | Yes                               | Defines `manual`, `time`-, `data`- or `metrics`-driven trigger. |
| `triggers[].value`   | integer   | Only for `time` and `data` types. | Value to activate trigger (in hours for `time` and number of new registries for  `data`). |
| `triggers[].fields`  | array of string | Only for `metrics` Type.  | List of metrics/statistics in Production to be compared in order to activate trigger.|

Example:
~~~json
{
  "model-name": "anemia.onnx",
  "pre-trained": "yes",
  "data-type": "tabular",
  "data": {
    "observation": {
      "laboratory-results": [
        "Hemoglobin",
        "MCH",
        "MCHC",
        "MCV"
      ],
      "vital-signs": [
        "weight",
        "respiratory-rate",
        "heart-rate"
      ]
    },
    "condition": [
      "anemia"
    ],
    "patient": [
      "gender"
    ]
  },
  "triggers": [
    {
      "type": "manual"
    }
  ],
  "hyperparameters": {
      "learning-rate": "optimal"
  }
}
~~~

## REST APIs

## ML Pipeline REST APIs

### Activate Trigger

> v1.1.0

This method initiates the training process to a specified ML model.

```plaintext
POST /trigger/{model_name}
```

Supported attributes:
| Attribute  | Type | Required | Description  |
| :--------- | :------- | :------- | :----------- |
| `model_name` | string | Yes      | Name of ML model to be trained. |

If successful, returns `<200>` and a string message as reponse.

### Evaluate

> v1.1.0

Evaluates a model according to a policy, returns a boolean that tells whether it should replace the current model.

```plaintext
POST /evaluate/{model_name}
```
<br/>

Supported attributes:
| Attribute  | Type | Required | Description  |
| :--------- | :------- | :------- | :----------- |
| `model_name` | String | Yes      | Name of the model to evaluate. Must be configured. |
| `res_dict`     | Dict | Yes     | Performance metrics of model(s) in repository. |

If successful, returns [`<200>`](../../api/index.md#200) and the following
response attributes:

| Attribute                | Type     | Description                                                       |
|:-------------------------|:---------|:----------------------                                            |
| `is-better`                 | Boolean  | Tested ML model is better than current production model        |


Policy attribute:

Is acquired through the model configuration and will be parsed according to the following regex:

    ((metric_1|metric_2|...|metric_n):(avg|median|75percentile|top);)+

Which can be interpreted as follows:

| Attribute                | Description                                                       |
|:-------------------------|:----------------------                                            |
| `metric_x`                 | One of *n* metrics in `json[metrics]`, except for `confusion-matrix` and `precision-and-recall` |                                         |
| `avg`                 | Select model if its `metric_x` is higher than *average* |   
| `median`                 | Select model if its `metric_x` is higher than the *median* |    
| `75percentile`                 | Select model if its `metric_x` is higher than 75% of models' |    
| `top`                 | Select model if its `metric_x` is the highest |    

<br/>

### Update Current Production Model
> v1.1.0

After evalution, if the model is indeed better than the current one in production, it should be replaced, this endpoint will call the plugin API, which should, by the requirements definition, have a endpoint that can update its current model for the one receive as paramether (docementation of it in the section below) without downtime.
This endpoint also tests the onnx file to make sure it will work in the plugin using the ```onnx``` python library built-in function ```load()```

```plaintext
POST /update-model/{plugin}
```

Supported attributes:
| Attribute  | Type | Required | Description  |
| :--------- | :------- | :------- | :----------- |
| `model` | Binary (.onnx) | Yes      | Model to send to plugin. |

If returns `<status-code>` and the message from the plugin API, regarding the updation successfulness.

<br/>

## Plugins REST APIs
> All plugins must provide a docker image that opens a local server with the following routes

### Training

```plaintext
POST /train
```
Supported attributes:

| Attribute  | Required | Type        | Description  |
| :--------- | :------- | :-----   | :----------- |
| `weights`     | Yes     | String | Address for the weight values of the pre-trained model. Is .pth or onnx file |
| `data` | Yes      | Object | Have the data to train the model. Each model will have its specific training data that can be image, tabular, etc|
| `hyperparameters` | Yes      | Dict | Hyperparameters required by ML model for training. Is a .json file|

**Train Response**

 Attribute  | Type        | Description  |
| :--------- | :------- | :----------- |
| `weights_address`   | String | Address of the new trained weights |

### Testing

```plaintext
POST /test
```

Supported attributes:
| Attribute  | Required | Type        | Description  |
| :--------- | :------- | :-----      | :----------- |
| `weights`     | Yes     | String | Address for the weight values of the model to test. Is .pth or onnx file |
| `data` | Yes      | Object | Have the data to test the model. Each model will have its specific data, can be image,tabular, etc|

**Test Response**

 Attribute   | Type        | Description  |
| :--------- | :------- | :----------- |
| `metrics`  | Dict | Performance metrics of the tested ML model according to the definition in the config file |

### Predicting

```plaintext
POST /predict
```

Supported attributes:

| Attribute  | Required | Type        | Description  |
| :--------- | :------- | :-----      |:----------- |
| `data`     | Yes      | Object      |The input to make prediction, changes depending of the data model.        |


**Predict Response**

| Attribute                | Type     | Description                                                       |
|:-------------------------|:---------|:----------------------                                            |
| `output`                   | Dict   | Returns the output of the prediction                              |



### Update Production Model

```plaintext
POST /update-prod-model
```
Supported attributes:

| Attribute  | Required | Type        | Description  |
| :--------- | :------- | :-----      |:----------- |
| `model`    | Yes      | Bytes        |The model.(Pre-trained model). Is .pth or onnx file |

**Update Production Model Response**

| Attribute                | Type     | Description                                                       |
|:-------------------------|:---------|:----------------------                                            |
| `message`                   | String   | A message regarding the successfulness of the updation      |


# Usage Examples and Tutorial

> Present here a brief tutorial or a set of selected usage examples of your subproject.

## ML Pipeline

### Activate Trigger

An example is to activate training and testing flow for the previous mentioned tabular data model. In order to do that, after executing commands of the section [Installation and Execution](#installation-and-execution), send a POST request to `http://localhost:4010/trigger/anemia` (no body request needed) and wait until process finish. At that point, both training and testing will have been already executed.

<br/>

### Evaluate

To use evaluate, set the policy in the model's configuration. As an example, consider the following policy:

`accuracy:median;f1-score:75percentile;au-roc:top;`

Then, sending a POST to `/evaluate/{model_name}` sets API output `is-better` `True` if the testing models' accuracy is higher than median, its f1-score is within the 75th percentile and has the highest au-roc score.

<br/>

### Updating Production Model

To test this functionality there is a script that simulates the entire flow, its ```/ML Pipeline/router/update-model-script.py``` 
<br/>
The script downloads the new model from the repository, updates the current one and then tests it calling the predict endpoint while displaying logs in the command line so the user can see whats happening.
<br/>
After following the steps in [Installation and Execution](#installation-and-execution), simply execute the python in the command line.

<br/>

### Predict

To test this functionality you can run the Docker container (in the puglin section have a explanation of how to do this) , and then when the container is up you can send as in the example below: 

**Predict example**
~~~shell
curl -X 'POST' 'http://0.0.0.0:4011/predict' -H 'accept: application/json' -H 'Content-Type: application/json' -d @data.json
~~~


# Highlights

## Image Object Detection Plugin

To illustrate how inference with an image model would work inside our architecture, an image object detection model was implemented as a ML plugin. For that we used and [already trained model that detects three kinds of blood cells](https://github.com/MahmudulAlam/Automatic-Identification-and-Counting-of-Blood-Cells):  WBC, RBC and Platelets.

This model was originally trained using the Darknet [YOLO](https://pjreddie.com/darknet/yolo/) and the inference is made using Tensorflow as backend via [Darkflow](https://github.com/thtrieu/darkflow), the authors provided an inference code but it only workt with a specific version of Tensorflow and Cython, thus just testing if the model was working well was already a challenge. However, converting it to .onnx in order to optimize inference a little hacking was needed, that's why it is highlighted here. Since we had only the darkflow weights (official support is only darknet->onnx or tensorflow->onnx) the steps were:

1. After instantiating the TFNet object inside the inference code, generate the .pb (protobuf file) and .meta (Tensorflow graph) files using the [.savepb()](https://github.com/thtrieu/darkflow/blob/master/darkflow/net/build.py) method of the object;
2. Using the [summarize graph](https://github.com/tensorflow/tensorflow/tree/master/tensorflow/tools/graph_transforms) tool, find out what are the names of the inputs and outputs that were used for training;
3. With those names and the .pb file, use the [tf2onnx](https://github.com/onnx/tensorflow-onnx) package to generate the .onnx model, the used command was:
~~~shell
python -m tf2onnx.convert --graphdef built_graph/tiny-yolo-voc-3c.pb --output model.onnx --inputs input:0 --outputs output:0
~~~

To make the output of the model usable, it was necessary to convert it to JSON, making it from the raw values would be too time demanding and thus a Tensorflow framework instance was created from the model Darknet object:
~~~python
class BloodCellsModel(object):
  def __init__(self, args): 
  ...
  self._options = {
            'model':args.model_cfg,
            'threshold': args.threshold,
            'labels': args.labels_path
        }
  new_options = argHandler()
  new_options.setDefaults()
  new_options.update(self._options)
  self._options = new_options
  darknet = Darknet(self._options)
  args_net = [darknet.meta, self._options]
  self._framework = create_framework(*args_net)
  ...
~~~
Allowing us to use the method process bbox:

~~~python
def process_outputs(self, outputs):
  ...
  for box in boxes:
    tmpBox = self._framework.process_box(box, self._args.img_size[0], self._args.img_size[1], self._options['threshold'])
    if tmpBox is None:
        continue
    boxesInfo.append({
        "label": tmpBox[4],
        "confidence": float(tmpBox[6]),
        "topleft": {
            "x": int(tmpBox[0]),
            "y": int(tmpBox[2])},
        "bottomright": {
            "x": int(tmpBox[1]),
            "y": int(tmpBox[3])}
    })
  return boxesInfo
  ...
~~~ 


## ML Pipeline

A differential point of this pipeline that must be highlighted is the translation and filtering of data. Which means that, to be processed in training and testing steps, data recovered are filtered so that all the diagnostics/patients used have the necessary information explicitly needed by model in its configuration file. And that's where translation step happens: daily terms used in configurtion file must bu converted to its LOINC/SNOMED code in order to establish integration between modules.

The generality of this solution must also be pointed. Thanks to trainging, testing and validating steps being part of a model requirements, just as configuration file with dataset specified, it was possible to deliver a generic solution for, at least, tabular data-oriented ML models.

## Data Models definiton

Data Models defenitons is a Highlight in the MLComponents, because we can define two Datas Structures one for Tabular ML Models and onde for Images ML Models that we can reuse for any Model that one day may be added to the pipeline.The Plugin manager can check the datas that we use in this implentation an do something similar that we have in the documentation.
Especially for Models that use tabular data, the Tabular Data Obejct has all the exams of a patient and has all patient informations, so the Plugin maneger have just to check the informations the Puglin need, after doing this check the data can be sent to the plugin and the ML can do its job.

## Production model updation

The are two big advantages of this functionality:
1. The model in production will always be the best one, based on the results of the evaluation.
2. The replacement of the model has no (almost) downtime, meaning it will be transparent for the final user.
