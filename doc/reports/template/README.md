# Where to place your report

Each subproject will create a subfolder below the folder `[/doc/reports]` in the `dish-main` repository. In this folder, there are all the files of the report (including images).

A `README.md` has to be in the root of this subproject folder, following the template presented in *Template for Final Report*.

# About the Project Template

Below is the model to document the final report. Anything between `<...>` indicates something that should be replaced by the indicated one.
> In addition, everything appearing in this citation mode also refers to something that must be replaced by the indicated one (without citation).

There are illustrative examples in the template, which will be replaced by those in your report.

# Template for Final Report

# Project `<Title>`

# Description
> Brief description of this subproject's role in the main project.

# Team
* `<complete member name>`
  * `<brief description of the activities developed by this member>`
* `<complete member name>`
  * `<brief description of the activities developed by this member>`

# GitLab Subproject
* `<link to the GitLab subproject>`

# Folder and Files Structure

> Each subproject must present its folder/file organization. It is unnecessary to document every folder and file; just the most significant to understand the project structure. See the following example:

~~~
├── README.md          <- project presentation
│
├── data
│   ├── external       <- third-party data
│   ├── interim        <- intermediary data, e.g., transformation results
│   ├── processed      <- final data for the model
│   └── raw            <- original data without modifications
│
├── notebooks          <- Jupyter notebooks or equivalents
│
├── src                <- source code in a program language or system
│   └── README.md      <- basic instructions for installation and execution
│
└── assets             <- media adopted in the project
~~~

# General Architecture or Process

> Present here a diagram with the general architecture or process, as the example:

![Architecture Example](images/architecture-example.png)

# Installation and Execution

> Detail here, step by step, how to install and execute your subproject. Whenever instructions must be executed, use the proper markdown syntax highlight as in the following example. For a list of supported languages visit the [Rouge project wiki](https://github.com/rouge-ruby/rouge/wiki/List-of-supported-languages-and-lexers).

~~~shell
cd git/project/
docker-compose up -d project-database project-database-ui
~~~

# Data Models and APIs

## Data Models

> If your subproject involves relational databases, present a relational diagram of your database. We suggest the use of the [MySQL Workbench](https://www.mysql.com/products/workbench/), which can automatically extract this diagram from a database schema, to be further visually organized, as the following example:

![Relational Schema Example](images/relational-schema-example.png)

> If your subproject involves JSON repositories, present a JSON structure following the JSON format defined in [Gitlab's Documenting REST API resources](https://docs.gitlab.com/ee/development/documentation/restful_api_styleguide.html), as follows:

~~~json
[
  {
  }
]
~~~

> In both cases (relational and JSON), the previous schemas must be followed by a table detailing each attribute, derived from [Gitlab's Documenting REST API resources](https://docs.gitlab.com/ee/development/documentation/restful_api_styleguide.html), as follows:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `attribute`              | datatype | Detailed description. |
| `attribute` **(<tier>)** | datatype | Detailed description. |

## REST APIs

> If the subproject involves the definition of REST APIs, they must be documented here., following [Gitlab's Documenting REST API resources](https://docs.gitlab.com/ee/development/documentation/restful_api_styleguide.html).

# Usage Examples and Tutorial

> Present here a brief tutorial or a set of selected usage examples of your subproject.

# Highlights

> Choose the differential of your project to highlight. You can explain your differential with a text and diagram, or present a clipping (you can use ellipses to remove less important parts). See an example of highlighting a clipping in Javascript:

~~~javascript
async start () {
  ...
  for (const knot in compiled.knots) {
    const mkHTML =
      await Translator.instance.generateHTML(compiled.knots[knot])
    html += mkHTML
  }
  ...
}
~~~

> Present a brief description of why this part of your project deserves a highlight. Since projects are complex, highlights are fundamental to guide the reader attention.
