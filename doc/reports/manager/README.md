# Final Report

# Project `Gestão de Exames e Pacientes - Suco do Bandeco`

# Description

> Our group was responsible for creating and managing all the project's databases, the most important ones being exams, patients and encounters.
> 
> The creation of the database was done through MongoDB, while the CRUD operations necessary to interact with it were done using Python code that communicated with the database through the pymongo library.
> 
> Finally, we used FastAPI to generate a webpage with the Swagger documentation, allowing other groups to access the database and perform the CRUD operations that were most appropriate for their areas.


# Team
* `Andre Vila Nova Wagner da Costa`
  * `Creation of the structure of all request bodies and the callbacks of the initial version of the API (Schemas folder).`
  * `Updated project REST API documentation.`
  * `Updated PUT and DELETE methods of Allergies, Medication and Procedures`
  * `Final code testing`
* `Breno Nunes Tavares`
  * `Docker and API testing/implementation`
  * `API classes structuring (modeling)`
  * `Adaptation of CRUD methods so they were compatible with changes made by other groups (e.g., PUT methods)`
  * `Updated project REST API documentation`
* `Eduardo Carvalheira Teixeira de Aguiar`
  * `Creation of the basic model of the MongoDB implementation with the Fast API`
  * `Docker Implementation`
  * `Testing server creation`
  * `Comunication with other teams to identify errors`
  * `Bug correction on the CRUD methods`
  * `Pagination on API's GET method`
* `Enrico Piovesana Fernandes`
  * `API routing structuring (routing)`
  * `Creation and initialization of the API's CRUD methods`
  * `Adaptation of CRUD methods so they were compatible with changes made by other groups (e.g., fixed duplicated IDs)`

# GitLab Subproject
* `https://gitlab.com/open-dish/manager`

# API Website (Will be shut down in the future)
* `https://dhim.api.open-dish.lab.ic.unicamp.br/`

# Folder and Files Structure

> Each subproject must present its folder/file organization. It is unnecessary to document every folder and file; just the most significant to understand the project structure. See the following example:

~~~
│
├── config
│   └── db.py          <- config file for database access
│
├── models             <- Models used by FASTAPI to structure documents
│   ├── allergy.py     <- Allergy Class structure
│   ├── diagnostic.py  <- Diagnostic Class structure
│   ├── encounter.py   <- Encounter Class structure
│   ├── image.py       <- Image Class structure
│   ├── medication.py  <- Medication Class structure
│   ├── patient.py     <- Patient Class structure
│   └── procedure.py   <- Procedure Class structure
│
├── routes             <- Routes define the available methods available in the API for each type of document
│   ├── allergy.py     <- Allergy Class API routes
│   ├── diagnostic.py  <- Diagnostic Class API routes
│   ├── encounter.py   <- Encounter Class API routes
│   ├── image.py       <- Image Class API routes
│   ├── medication.py  <- Medication Class API routes
│   ├── patient.py     <- Patient Class API routes
│   └── procedure.py   <- Procedure Class API routes
│
├── schemas            <- Schemas used to validate submitted documents
│   ├── allergy.py     <- Allergy Class schemas
│   ├── diagnostic.py  <- Diagnostic Class schemas
│   ├── encounter.py   <- Encounter Class schemas
│   ├── image.py       <- Image Class schemas
│   ├── medication.py  <- Medication Class schemas
│   ├── patient.py     <- Patient Class schemas
│   └── procedure.py   <- Procedure Class schemas
│
├── .dockerignore
├── .gitignore 
├── .gitlab-ci.yml     <- YML file
├── Dockerfile         <- Dockerfile used to create an Local Server to Acess the REST API
├── build.sh           <- Script to Build the Docker Container
├── compose.yml        <- Docker Compose YML
├── index.py           <- Main file used to start up FastAPI and include all the methods defined in /routes filer
├── package-lock.json
├── requirements.txt   <- Libraries used in the Docker Container
├── start.sh           <- Script to Run the Docker Container
├── stop.sh            <- Script to Stop the Docker Container
│
└── README.md          <- project presentation
~~~

# General Architecture or Process

![Architecture Diagram](images/suco-do-bandeco-diagram.png)

# Installation and Execution

> All the installation and execution are done automatically by running scripts that build, enable or disable docker containers. To run the REST API Locally, follow the steps:
> * Execute the build.sh script to build the Docker  Container 
> * Execute the start.sh script to run the container

~~~shell
./build.sh
./start.sh
~~~

> If it doesn't work, try:

~~~shell
source build.sh
source start.sh
~~~

> * Execute the stop.sh to stop using the container
 
~~~shell
./stop.sh
~~~

> If it doesn't work, try:

~~~shell
source stop.sh
~~~

## Data Model and REST APIs

> All the Data Model used and REST APIs methods implemented are available at the following link: [Management REST API](https://gitlab.com/open-dish/dish/-/blob/development/doc/api/management.md).

# Usage Examples and Tutorial

> Present here a brief tutorial or a set of selected usage examples of your subproject.

# Highlights

### Folder Organization
> The API code was organized in three folders:
> * models - class modeling and definitions
> * routes - CRUD methods implementation
> * schemas - data formatting for the API and test validation
> The Folder Organization can be better visualized at the [Folder and Files Structure](#folder-and-files-structure) section.

### Pagination on the Get Method

> Implemented a pagination system so the GET commands wouldn't make the server and API request slow. It helped the Front-end team and the Synthetic Data Generation team.

~~~python
@diagnostic.get('/diagnostic/')
async def find_all_diagnostics(pageNumber: int = 1, nDiagnostic: int = 15):
    total = int(db.command("collstats", "diagnostic")["count"])
    totalPages = total//nDiagnostic+1
    numberDiagnostics = total - nDiagnostic*(totalPages-1) if totalPages-pageNumber == 0 else nDiagnostic
    if pageNumber > totalPages:
        raise HTTPException(status_code=400, detail="Page doesn't exist.")
    return {{ "Actual Page": pageNumber, "Total Pages": totalPages, "Number of Diagnostics": numberDiagnostics },
            serializeList(db.diagnostic.find().skip((pageNumber-1)*nDiagnostic).limit(nDiagnostic))}
~~~

> The Python method above is responsible for the pagination described earlier. It is possible to notice that there are two optional parameters, "pageNumber", which receives the page that the user wants to consult, and "nDiagnostic" (or "nClassName", depending on the Class) that determines how much data will be displayed per page.

### Good Communication

> Team was accessible at all times at Discord and at the classroom. It was usefull to fix the REST API bugs and align with other Teams necessities. Resulting in cleaner and easy execution of the endpoints.

### GET and PUT methods

> Implemented multiple GET methods, by patient or by encounter, for example. Also, implemented custom PUT methods that were requested by other teams.

~~~python
@image.put('/image/box/{id}')
async def insert_bounding_box(id,box: List[Boxes], response: Response):
    img = db.image.find_one({"id":id})
    if img == None:
        raise HTTPException(status_code=404, detail="Image doesn't exist.")
    if img["boxesInfo"] == None:
        img["boxesInfo"] = jsonable_encoder(box)
    else:
        img["boxesInfo"] = img["boxesInfo"] + jsonable_encoder(box)
    db.image.find_one_and_update({"id":id},{
        "$set":{
            "boxesInfo": img["boxesInfo"]
        }
    })
    response.status_code = status.HTTP_201_CREATED
    return serializeDict(db.image.find_one({"id":id}))
~~~

> The Python method above is one of the custom methods requested by another team that was implemented in order to allow for bounding boxes to be saved and associated with images in the database.

### Up to date docummentation

> The team tried to continuously keep the documentation available on the GitLab repository updated and as easy to read and understand as possible so other groups could read it and understand our really extensive API. 
>
> The Data Model and REST APIs was better explained at the [Data Model and REST APIs](#data-model-and-rest-apis) section.
