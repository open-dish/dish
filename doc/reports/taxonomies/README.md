# Project Taxonomies

# Description
> This project wants to facilitate the queries about dictionaries and nomenclatures including SNOMED descriptions, RXNORM medication and Loinc observations.

# Team
* `Gabriel Francioli Alves`
  * `Work on api development and some interactions with database.`
* `Thiago Lima Costa`
  * `Work understanding the databases and making some models.`

# GitLab Subproject
* `https://gitlab.com/open-dish/taxinomies`

# Folder and Files Structure

> The structure of the project follow the model:

~~~
├── README.md          <- project presentation
│
└── prisma             <- ORM adopted in the project
    ├── migrations     <- folder of migrations
    └── schema.prisma  <- file of the database models
|
├── src
│   ├── README.md      <- basic instructions for installation and execution
│   ├── config         <- ORM database config
│   ├── controllers    <- controller of api, manage routes and call repositories
│   ├── interceptors   <- the place of some middleware that can be applied
│   ├── repositories   <- repositories of tables that are in database
│   └── models         <- DTO of the requests, in most cases follow database table model
~~~

# General Architecture or Process

![Architecture Example](/doc/reports/taxonomies/images/diagram.drawio.png)

# Installation and Execution

## Requirements

- Install [node](https://nodejs.org/en/download/) <b> version 14 </b>
- Install [yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable)
- Install [docker](https://docs.docker.com/install/)
- Install [docker-compose](https://docs.docker.com/compose/install/)

### Prepare your development environment

Create a copy .env file from .env.example and populate the variables.

Build and install the dependencies:

## Installation with docker

```bash
# Navigate to the root of the project and run on a terminal

$ make up # up the docker image from the database

$ npx prisma migrate dev # installs pending database migrations, it is only necessary the first time

$ sh build.sh # build container api

```

## Running the app local

```bash
# Navigate to the root of the project and run on a terminal
$ yarn # installs project dependencies

$ make up

$ npx prisma migrate dev # installs pending database migrations, it is only necessary the first time

# development
$ yarn start # execute the start script in package.json, start the project

# watch mode
$ yarn start:dev # execute the project in watch mode

# production mode
$ yarn start:prod  # executes the project for production
```


# Data Models and APIs

## Data Models

![Relational Schema Example](/doc/reports/taxonomies/images/test%20-%20public.png)

### Snap Description

Model referring to the SNOMED vocabulary, containing the main attributes related to the concepts.

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | Integer | Primary key, SNOMED concept identifier. |
| `effectiveTime`              | Date (`YYYY-MM-DD`) | Concept effective time. The most up-to-date concepts have the most recent effective times. |
| `active`              | Integer | Whether the concept is used or not. |
| `modeluId`              | Integer | Concept module identifier. |
| `conceptId`              | Integer | Concept identifier. |
| `languageCode`              | String | Language code used. |
| `typeId`              | Integer | Display configuration type used for this concept. |
| `term`              | String | Term description. |
| `caseSignificanceId`              | Integer | Configuration identifier for specific cases. |

### Medication

Medication model using the RXNorm vocabulary for drugs.

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | Integer | Primary key, medication identifier. |
| `RXCUI`              | String | RxNorm Unique identifier for concept (concept ID). |
| `LAT`               | String | Language of Term. |
| `TS`               | String | Term status. |
| `LUI`               | String | Unique identifier for term. |
| `STT`               | String | String type. |
| `SUI`               | String | Unique identifier for string. |
| `ISPREF`               | String | Atom status - preferred (Y) or not (N) for this string within this concept. |
| `RXAUI`               | String | Unique identifier for atom (RxNorm Atom ID). |
| `SAUI`               | String | Source asserted atom identifier (optional). |
| `SCUI`               | String | Source asserted concept identifier (optional). |
| `SDUI`               | String | Source asserted descriptor identifier (optional). |
| `SAB`               | String | Source abbreviation (abbreviation in the source vocabulary from which it was taken). |
| `TTY`               | String | Term type in source. |
| `CODE`               | String | "Most useful" source asserted identifier (if the source vocabulary has more than one identifier), or a RxNorm-generated source entry identifier (if the source vocabulary has none). |
| `STR`               | String | String (the term itself). |
| `SRL`               | String | Source Restriction Level. |
| `SUPPRESS`               | String | Suppressible flag. Values = N, O, Y, or E. N - not suppressible. O - Specific individual names (atoms) set as Obsolete because the name is no longer provided by the original source. Y - Suppressed by RxNorm editor. E - unquantified, non-prescribable drug with related quantified, prescribable drugs. NLM strongly recommends that users not alter editor-assigned suppressibility. |
| `CVF`               | String | Content view flag. RxNorm includes one value, '4096', to denote inclusion in the Current Prescribable Content subset. |

### Observations

Model of clinical observations with LOINC. As it has a wide range of observations, we have kept the large number of attributes. 

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | Integer | Primary key, observation identifier. |
| `LOINC_NUM`              | String | The unique LOINC Code is a string in the format of nnnnnnnn-n. |
| `COMPONENT`              | String |  The substance or entity being measured or observed. First major axis-component or analyte. |
| `PROPERTY`              | String | The characteristic or attribute of the analyte. Second major axis-property observed (e.g., mass vs. substance). |
| `TIME_ASPCT`              | String | The interval of time over which an observation was made. Third major axis-timing of the measurement (e.g., point in time vs 24 hours). |
| `SYSTEM1`              | String |  The specimen or thing upon which the observation was made. Fourth major axis-type of specimen or system (e.g., serum vs urine).  |
| `SCALE_TYP`              | String | How the observation value is quantified or expressed: quantitative, ordinal, nominal. Fifth major axis-scale of measurement (e.g., qualitative vs. quantitative). |
| `METHOD_TYP`              | String | A high-level classification of how the observation was made. Only needed when the technique affects the clinical interpretation of the results. Sixth major axis-method of measurement. |
| `VersionLastChanged`              | String | The LOINC version number in which the record has last changed. For records that have never been updated after their release, this field will contain the same value as the loinc.VersionFirstReleased field. |
| `DefinitionDescription`              | String | Narrative text that describes the LOINC term taken as a whole (i.e., taking all of the parts of the term together) or relays information specific to the term, such as the context in which the term was requested or its clinical utility. |
| `STATUS1`              | String | ACTIVE = Concept is active. Use at will. TRIAL = Concept is experimental in nature. Use with caution as the concept and associated attributes may change. DISCOURAGED = Concept is not recommended for current use. New mappings to this concept are discouraged; although existing may mappings may continue to be valid in context. DEPRECATED = Concept is deprecated. Concept should not be used, but it is retained in LOINC for historical purposes. |
| `CONSUMER_NAME`              | String | An experimental (beta) consumer friendly name for this item. The intent is to provide a test name that health care consumers will recognize. |
| `CLASSTYPE`              | String | 1=Laboratory class; 2=Clinical class; 3=Claims attachments; 4=Surveys. |
| `FORMULA`              | String | Contains the formula in human readable form, for calculating the value of any measure that is based on an algebraic or other formula except those for which the component expresses the formula. So Sodium/creatinine does not need a formula, but Free T3 index does. |
| `EXMPL_ANSWERS`              | String | For some tests and measurements, certain examples were have supplied of valid answers, such as “1:64”, “negative @ 1:16”, or “55”. |
| `SURVEY_QUEST_TEXT`              | String | Verbatim question from the survey instrument. |
| `SURVEY_QUEST_SRC`              | String | Exact name of the survey instrument and the item/question number. |
| `UNITSREQUIRED`              | String | Y/N field that indicates that units are required when this LOINC is included as an OBX segment in a HIPAA attachment. |
| `RELATEDNAMES2`              | String | This field was introduced in version 2.05. It contains synonyms for each of the parts of the fully specified LOINC name (component, property, time, system, scale, method). |
| `SHORTNAME`              | String | Introduced in version 2.07, this field contains the short form of the LOINC name and is created via a table-driven algorithmic process. The short name often includes abbreviations and acronyms. |
| `ORDER_OBS`              | String | Defines term as order only, observation only, or both. A fourth category, Subset, is used for terms that are subsets of a panel but do not represent a package that is known to be orderable. This field reflects our best approximation of the terms intended use; it is not to be considered normative or a binding resolution. |
| `HL7_FIELD_SUBFIELD_ID`              | String | A value in this field means that the content should be delivered in the named field/subfield of the HL7 message. When NULL, the data for this data element should be sent in an OBX segment with this LOINC code stored in OBX-3 and with the value in the OBX-5. |
| `EXTERNAL_COPYRIGHT_NOTICE`              | String | External copyright holders copyright notice for this LOINC code. |
| `EXAMPLE_UNITS`              | String | This field is populated with a combination of submitters units and units that people have sent us. Its purpose is to show users representative, but not necessarily recommended, units in which data could be sent for this term. |
| `LONG_COMMON_NAME`              | String | This field contains the LOINC name in a more readable format than the fully specified name. The long common names have been created via a tabledriven algorithmic process. Most abbreviations and acronyms that are used in the LOINC database have been fully spelled out in English. |
| `EXAMPLE_UCUM_UNITS`              | String | The Unified Code for Units of Measure (UCUM) is a code system intended to include all units of measures being contemporarily used in international science, engineering, and business. (www.unitsofmeasure.org) This field contains example units of measures for this term expressed as UCUM units. |
| `STATUS_REASON`              | String | Classification of the reason for concept status. This field will be Null for ACTIVE concepts, and optionally populated for terms in other status where the reason is clear. DEPRECATED or DISCOURAGED terms may take values of: AMBIGUOUS, DUPLICATE, or ERRONEOUS. |
| `STATUS_TEXT`              | String | Explanation of concept status in narrative text. This field will be Null for ACTIVE concepts, and optionally populated for terms in other status. |
| `CHANGE_REASON_PUBLIC`              | String | Detailed explanation about special changes to the term over time. |
| `COMMON_TEST_RANK`              | String | Ranking by frequency of usage of approximately 20,000 LOINCs codes as reported in the U.S. |
| `COMMON_ORDER_RANK`              | String | Ranking of approximately 300 common orders performed by laboratories in USA. |
| `COMMON_SI_TEST_RANK`              | String | SI ranks no longer provided within LOINC database. |
| `HL7_ATTACHMENT_STRUCTURE`              | String | This field will be populated in collaboration with the HL7 Attachments Work Group as described in the HL7. |
| `EXTERNAL_COPYRIGHT_LINK`              | String | For terms that have a third party copyright, this field is populated with the COPYRIGHT_ID from the Source Organization table (see below). It links an external copyright statement to a term. |
| `PanelType`              | String | Describes a panel as a "Convenience group", "Organizer", or "Panel". |
| `AskAtOrderEntry`              | String | A multi-valued, semicolon delimited list of LOINC codes that represent optional Ask at Order Entry (AOE) observations for a clinical observation or laboratory test. A LOINC term in this field may represent a single AOE observation or a panel containing several AOE observations. |
| `AssociatedObservations`              | String | A multi-valued, semicolon delimited list of LOINC codes that represent optional associated observation(s) for a clinical observation or laboratory test. A LOINC term in this field may represent a single associated observation or panel containing several associated observations. |
| `VersionFirstReleased`              | String | The LOINC version number in which the record was first released. For oldest records where the version released number is known, this field will be null. |
| `ValidHL7AttachmentRequest`              | String | A value of 'Y' in this field indicates that this LOINC code can be sent by a payer as part of an HL7 attachment request for additional information. |
| `DisplayName`              | String | This field contains a name that is more "clinician-friendly" compared to the current LOINC Short Name, Long Common Name, and Fully Specified Name. It is created algorithmically from the manually crafted display text for each Part and is generally more concise than the Long Common Name. |

## REST APIs and Usage Examples
### GET /medication/all
```shell
curl -X 'GET' \
  'http://localhost:3000/medication/all' \
  -H 'accept: application/json'
```
- Response:
```json
[
  {
    "id": 6,
    "RXCUI": "61",
    "LAT": "ENG",
    "TS": "",
    "LUI": "",
    "STT": "",
    "SUI": "",
    "ISPREF": "",
    "RXAUI": "12254378",
    "SAUI": "12254378",
    "SCUI": "61",
    "SDUI": "",
    "SAB": "RXNORM",
    "TTY": "IN",
    "CODE": "61",
    "STR": "beta-alanine",
    "SRL": "",
    "SUPPRESS": "N",
    "CVF": "4096"
  }
]
```


### POST /medication
```shell
curl -X --request POST 'localhost:3000/medication' \
--header 'Content-Type: application/json' \
--data-raw '{
    {
  "RXCUI": "string",
  "LAT": "string",
  "TS": "string",
  "LUI": "string",
  "STT": "string",
  "SUI": "string",
  "ISPREF": "string",
  "RXAUI": "string",
  "SAUI": "string",
  "SCUI": "string",
  "SDUI": "string",
  "SAB": "string",
  "TTY": "string",
  "CODE": "string",
  "STR": "string",
  "SRL": "string",
  "SUPPRESS": "string",
  "CVF": "string"
}
}'
```
- Response is the same object.

### GET /medication
```shell
curl -X 'GET' \
  'http://localhost:3000/medication?STR=beta-alanine&RXCUI=61' \
  -H 'accept: application/json'
```
- Response:
```json
[
  {
    "id": 6,
    "RXCUI": "61",
    "LAT": "ENG",
    "TS": "",
    "LUI": "",
    "STT": "",
    "SUI": "",
    "ISPREF": "",
    "RXAUI": "12254378",
    "SAUI": "12254378",
    "SCUI": "61",
    "SDUI": "",
    "SAB": "RXNORM",
    "TTY": "IN",
    "CODE": "61",
    "STR": "beta-alanine",
    "SRL": "",
    "SUPPRESS": "N",
    "CVF": "4096"
  }
]
```
### GET /observation/all
```shell
curl -X 'GET' \
  'http://localhost:3000/observation/all' \
  -H 'accept: application/json'
```
- Response:
```json
[
  {
    "id": 2,
    "LOINC_NUM": "100002-5",
    "COMPONENT": "Specimen care is maintained",
    "PROPERTY": "Find",
    "TIME_ASPCT": "Pt",
    "SYSTEM1": "^Patient",
    "SCALE_TYP": "Ord",
    "METHOD_TYP": "",
    "VersionLastChanged": "2.72",
    "DefinitionDescription": "",
    "STATUS1": "ACTIVE",
    "CONSUMER_NAME": "",
    "CLASSTYPE": "4",
    "FORMULA": "",
    "EXMPL_ANSWERS": "",
    "SURVEY_QUEST_TEXT": "",
    "SURVEY_QUEST_SRC": "",
    "UNITSREQUIRED": "N",
    "RELATEDNAMES2": "Finding; Findings; Ordinal; Point in time; QL; Qual; Qualitative; Random; Screen; Spec; Survey; SURVEY.PNDS",
    "SHORTNAME": "",
    "ORDER_OBS": "Observation",
    "HL7_FIELD_SUBFIELD_ID": "",
    "EXTERNAL_COPYRIGHT_NOTICE": "",
    "EXAMPLE_UNITS": "",
    "LONG_COMMON_NAME": "Specimen care is maintained",
    "EXAMPLE_UCUM_UNITS": "",
    "STATUS_REASON": "",
    "STATUS_TEXT": "",
    "CHANGE_REASON_PUBLIC": "",
    "COMMON_TEST_RANK": "0",
    "COMMON_ORDER_RANK": "0",
    "COMMON_SI_TEST_RANK": "0",
    "HL7_ATTACHMENT_STRUCTURE": "",
    "EXTERNAL_COPYRIGHT_LINK": "",
    "PanelType": "",
    "AskAtOrderEntry": "",
    "AssociatedObservations": "",
    "VersionFirstReleased": "2.72",
    "ValidHL7AttachmentRequest": "",
    "DisplayName": ""
  }
]
```

### POST /observation
```shell
curl --location --request POST 'localhost:3000/observation' \
--header 'Content-Type: application/json' \
--data-raw '{
    "LOINC_NUM": "string",
    "COMPONENT": "string",
    "PROPERTY": "string",
    "TIME_ASPCT": "string",
    "SYSTEM1": "string",
    "SCALE_TYP": "string",
    "METHOD_TYP": "string",
    "VersionLastChanged": "string",
    "DefinitionDescription": "string",
    "STATUS1": "string",
    "CONSUMER_NAME": "string",
    "CLASSTYPE": "string",
    "FORMULA": "string",
    "EXMPL_ANSWERS": "string",
    "SURVEY_QUEST_TEXT": "string",
    "SURVEY_QUEST_SRC": "string",
    "UNITSREQUIRED": "string",
    "RELATEDNAMES2": "string",
    "SHORTNAME": "string",
    "ORDER_OBS": "string",
    "HL7_FIELD_SUBFIELD_ID": "string",
    "EXTERNAL_COPYRIGHT_NOTICE": "string",
    "EXAMPLE_UNITS": "string",
    "LONG_COMMON_NAME": "string",
    "EXAMPLE_UCUM_UNITS": "string",
    "STATUS_REASON": "string",
    "STATUS_TEXT": "string",
    "CHANGE_REASON_PUBLIC": "string",
    "COMMON_TEST_RANK": "string",
    "COMMON_ORDER_RANK": "string",
    "COMMON_SI_TEST_RANK": "string",
    "HL7_ATTACHMENT_STRUCTURE": "string",
    "EXTERNAL_COPYRIGHT_LINK": "string",
    "PanelType": "string",
    "AskAtOrderEntry": "string",
    "AssociatedObservations": "string",
    "VersionFirstReleased": "string",
    "ValidHL7AttachmentRequest": "string",
    "DisplayName": "string"
}'
```
- Response is the same object.


### GET /observation
```shell
curl -X 'GET' \
  'http://localhost:3000/observation?COMPONENT=Specimen&LOINC_NUM=100002-5' \
  -H 'accept: application/json'
```
- Response:
```json
[
  {
    "id": 2,
    "LOINC_NUM": "100002-5",
    "COMPONENT": "Specimen care is maintained",
    "PROPERTY": "Find",
    "TIME_ASPCT": "Pt",
    "SYSTEM1": "^Patient",
    "SCALE_TYP": "Ord",
    "METHOD_TYP": "",
    "VersionLastChanged": "2.72",
    "DefinitionDescription": "",
    "STATUS1": "ACTIVE",
    "CONSUMER_NAME": "",
    "CLASSTYPE": "4",
    "FORMULA": "",
    "EXMPL_ANSWERS": "",
    "SURVEY_QUEST_TEXT": "",
    "SURVEY_QUEST_SRC": "",
    "UNITSREQUIRED": "N",
    "RELATEDNAMES2": "Finding; Findings; Ordinal; Point in time; QL; Qual; Qualitative; Random; Screen; Spec; Survey; SURVEY.PNDS",
    "SHORTNAME": "",
    "ORDER_OBS": "Observation",
    "HL7_FIELD_SUBFIELD_ID": "",
    "EXTERNAL_COPYRIGHT_NOTICE": "",
    "EXAMPLE_UNITS": "",
    "LONG_COMMON_NAME": "Specimen care is maintained",
    "EXAMPLE_UCUM_UNITS": "",
    "STATUS_REASON": "",
    "STATUS_TEXT": "",
    "CHANGE_REASON_PUBLIC": "",
    "COMMON_TEST_RANK": "0",
    "COMMON_ORDER_RANK": "0",
    "COMMON_SI_TEST_RANK": "0",
    "HL7_ATTACHMENT_STRUCTURE": "",
    "EXTERNAL_COPYRIGHT_LINK": "",
    "PanelType": "",
    "AskAtOrderEntry": "",
    "AssociatedObservations": "",
    "VersionFirstReleased": "2.72",
    "ValidHL7AttachmentRequest": "",
    "DisplayName": ""
  }
]
```
### GET /observation/related
```shell
curl -X 'GET' \
  'http://localhost:3000/observation/related?keyword=heart' \
  -H 'accept: application/json'
```
- Response:
```json
[
  {
    "LOINC_NUM": "100230-2",
    "COMPONENT": "Routine prenatal assessment panel",
    "LONG_COMMON_NAME": "Routine prenatal assessment panel",
    "RELATEDNAMES2": "H+P; P prime; Pan; PANEL.H&P; Panl; Pnl; Point in time; Random; Routine prenatal pnl",
    "SYSTEM1": "^Patient",
    "DefinitionDescription": "This panel contains terms to record the routine observations that occur during prenatal visits, such as weight, fundal height, urine protein, glucose, and albumin, and fetal heart rate. [PMID: 24506122]",
    "SHORTNAME": "Routine prenatal pnl",
    "DisplayName": ""
  }
]
```

### GET /snomed/related
```shell
curl -X 'GET' \
  'http://localhost:3000/snomed/related?term=neurologic' \
  -H 'accept: application/json'
```
- Response:
```json
[
  {
    "id": "2520014",
    "effectiveTime": "2002-01-31T00:00:00.000Z",
    "active": 0,
    "moduleId": "900000000000207008",
    "conceptId": "866003",
    "languageCode": "en",
    "typeId": "900000000000013009",
    "term": "Diabetic neuropathy with neurologic complication",
    "caseSignificanceId": "900000000000020002"
  }
]
```

### GET /snomed
```shell
curl -X 'GET' \
  'http://localhost:3000/snomed?id=900000000000207008' \
  -H 'accept: application/json'
```
- Response:
```json
[
  {
    "id": "208188014",
    "effectiveTime": "2002-01-31T00:00:00.000Z",
    "active": 1,
    "moduleId": "900000000000207008",
    "conceptId": "100005",
    "languageCode": "en",
    "typeId": "900000000000013009",
    "term": "SNOMED RT Concept",
    "caseSignificanceId": "900000000000017005"
  }
]
```

# Highlights

### Prisma
> This project uses [Prisma](https://www.prisma.io/docs/) to manage the database. Prisma's main goal is to make application developers more productive when working with databases. Considering the tradeoff between productivity and control again, this is how Prisma fits in:
![Prisma demo](/doc/reports/taxonomies/images/prisma.png)
Prisma structure make so easy to declare table models and relations:
~~~javascript
generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model medication {
  id       Int    @id @default(autoincrement())
  RXCUI    String
  LAT      String @default("ENG")
  TS       String
  LUI      String
  STT      String
  SUI      String
  ISPREF   String
  RXAUI    String
  SAUI     String
  SCUI     String
  SDUI     String
  SAB      String
  TTY      String
  CODE     String
  STR      String
  SRL      String
  SUPPRESS String
  CVF      String
}
~~~
### Nest.JS
>[Nest.JS](https://docs.nestjs.com/) as framework. The framework is built using TypeScript which allows developers to build highly scalable and testable applications. The framework contains a built-in dependency injection, this feature makes applications modular and efficient, provide a detailed documentation, easy integration with multiples ORM's and [Swagger](https://swagger.io/) documentation.
![Nest](/doc/reports/taxonomies/images/nestframework.png).

### Swagger
>[Swagger](https://swagger.io/) facilitates the documentation of API's, this tool allows you to easily understand what's the API needs to execute, how it works and how is the response.
~~~javascript
@ApiOperation({
    summary: "Get medication.",
  })
  @ApiQuery(
    {
      name: "RXCUI",
      required: false
    }
  )
  @ApiQuery(
    {
      name: "STR",
      required: false
    }
  )
  @ApiResponse({ type: MedicationRequest })
  ~~~
>Swagger allows you to test the API directly from the documentation too.
![Swagger](/doc/reports/taxonomies/images/swagger2.jpeg)

### Division of tasks
>Initially we were in 4 people and the job was well defined between us. In the end, we was a pair and we had to discuss what each one would do. We had a good division of tasks even in two people and the job was easier to do.


