# Project: `Tests for API's`

# Description
Create tests for ML Services, ML Components and Data Management APIs, aiming track possible issues and bugs between the calling of its endpoints. The test cases are placed between the Build and Deploy steps inside the CI/CD pipeline of each project.

To achieve our goal to create tests for each API, we separated our tasks into four stages:
- Requirement Analysis
- Test Planning
- Test Development
- Test Excecution

We detailed each stage in our [Test Wiki](https://gitlab.com/open-dish/dish/-/wikis/tests).

![Lifecycle](https://gitlab.com/open-dish/dish/-/wikis/uploads/2440d1961f2896a5fa8a3a98a3ac481e/stlc.jpeg)

# Team
* Michell Henrique Lucino
  * Created test scripts and schemas for ML Services API, improved project documentation in Wiki
* José Augusto Nascimento Afonso Marcos
  * Created test scripts and schemas for ML Components API, created CI/CD pipeline in Datasci repository
* Leonardo Almeida Reis
  * Created test scripts and schemas for Data Management API, created CI/CD pipeline in Data Management repository


# GitLab Subproject

Here we have each module that we've created the test cases. For both ML Components and ML Services, we've created inside Datasci repository. For Data Management, we've created inside Manager repository.

* [Datasci (ML) Repository](https://gitlab.com/open-dish/datasci)
* [Data Management Repository](https://gitlab.com/open-dish/manager)

# Folder and Files Structure

* Datasci structure

~~~
├── README.md          <- project presentation
│
├── tests              <- tests folder
|   |
│   ├── tests_ml_component       <- tests for ML Component API
|   |   ├── objects              <- sending objects for each API
|   |   ├── response_examples    <- response exemples for each API
|   |   ├── response_schemas     <- response schemas to validation
|   |   ├── tests_image          <- test scripts for image API
|   |   ├── tests_tabular        <- test scripts for tabular API
|   |   └── utils.py             <- general functions to all scripts
|   |
│   └── tests_ml_services        <- tests for ML Services API
|       ├── objects              <- sending objects for each API
|       ├── response_examples    <- response exemples for each API
|       ├── schemas              <- response schemas to validation
|       ├── tests                <- test scripts
|       └── utils.py             <- general functions to all scripts
|
├── .gitlab-ci.yml     <- YML file
│
└── API's Models Services        <- APIs

~~~

* Data Management structure

~~~
├── README.md          <- project presentation
│
├── tests              <- tests for Management API
|   |
|   ├── objects              <- sending objects for each API
|   ├── response_examples    <- response exemples for each API
|   ├── schemas              <- response schemas to validation
|   ├── tests                <- test scripts for Management API
|   └── utils.py             <- general functions to all scripts
|
├── .gitlab-ci.yml     <- YML file

~~~

# General Architecture or Process

Here is an example of the CI/CD process for the ML Services and ML Components API's. 

![CI/CD Processes](https://media.discordapp.net/attachments/1049452857352527903/1049522218691989504/image.png)

This process is executed automatically everytime that someone do a push in the project repository or manually executes by the [CI/CD](https://gitlab.com/open-dish/datasci/-/pipelines) option  inside Gitlab. Both Image Plugin and Tabular Plugin are tested by "TEST IMAGE AND TABULAR" job, while ML Services Plugin is tested by the "TEST ML SERVICES" job. Tests for the Data management plugin are made similarly, with a job in pipeline. Once the CI/CD process hits the test job, the following workflow is executed:

![Test Job Workflow](https://media.discordapp.net/attachments/1049452857352527903/1049522266892943370/image.png)

With a mocked object (done based on teams documentation), we call the endpoint and send it. The response is compared with a Schema, created by the test team, and with the expected response code, to ensure that all the fields in the json response and code match the requirements. Below, we have a example of a Success Test for the ML Services API on CI/CD:

~~~cmd
Ran 1 test in 0.012s
OK
{'area': 'audio', 'task': 'banana', 'description': 'This task extracts the name entities from a text.'}
RESPONSE FROM API
{'area': 'audio', 'task': 'banana', 'description': 'This task extracts the name entities from a text.'}
Schema OK!
Cleaning up project directory and file based variables
00:14
Job succeeded
~~~

Below, example of failed tests for the ML Components API on CI/CD:

Schema not as expected:

~~~cmd
{'detail': [{'loc': ['body', 'gender'], 'msg': 'field required', 'type': 'value_error.missing'}, {'loc': ['body', 'hemoglobin'], 'msg': 'field required', 'type': 'value_error.missing'}, {'loc': ['body', 'MCH'], 'msg': 'field required', 'type': 'value_error.missing'}, {'loc': ['body', 'MCHC'], 'msg': 'field required', 'type': 'value_error.missing'}, {'loc': ['body', 'MCV'], 'msg': 'field required', 'type': 'value_error.missing'}]}
ERRORS
{'detail': ['unknown field']}
~~~

Status code not as expected:

~~~cmd
Traceback (most recent call last):
  File "/builds/open-dish/datasci/tests/tests_ml_component/tests_tabular/test_post_train.py", line 38, in test_request
    validate_request(self)
  File "/builds/open-dish/datasci/tests/tests_ml_component/utils.py", line 52, in validate_request
    self.assertEqual(code,self.expected_code)
AssertionError: 404 != 400
~~~

# Installation and Execution

All tests run automatically via `.gitlab-ci.yml`

To run the tests locally, first you have to install the requirements.

~~~shell
cd tests
pip install -r requirements.txt
~~~

Then, to run the tests, go to the folder where the tests resides and execute the following:

~~~shell
python -m unittest -v tests/test_*.py
~~~

This command will execute all the tests. However, if want to execute an specific test, just specify the name of the file, like de following:

~~~shell
python -m unittest -v tests/test_filename
~~~

# Data Models

All of our validation is based on JSON schemas, used to compare the real response of the API with the expected result (the schema itself). Here, we have an example:

~~~JSON
{
    "evaluate":{
        "type": "dict",
        "schema": {
            "is_better": {"type": "boolean"}
        }
    }
}
~~~

Each schema is organized in similar way. Each class inside the JSON file have its `name` and `type`. In this case, we have a class with `name evaluate` and `type dict`. Each class that have `type list` or `dict` have the `schema` attribute, that denotes the form of that class. This pattern is used by Cerberus to identify how the response looks like. In this case, this schema denotes a response like:

~~~JSON
{
    "evaluate":{
        "is_better": false
    }

}
~~~

At [Cerberus page](https://docs.python-cerberus.org/en/stable/) is possible to see many other examples and full documentation. At [ML Services test - Schemas](https://gitlab.com/open-dish/datasci/-/tree/main/tests/tests_ml_services/schemas) is possible to see another examples that were implemented.

# Highlights

### Mocks

Mocks were very helpful to put things together in CI/CD.
  - Created dummy responses with [Mocky](https://designer.mocky.io/);
  - With mocks, we could write the tests before real APIs were ready, delivering code much faster;
  - With the tests functions working with mocked responses, we've had much more time and focus to make CI/CD work, and, just after that, change the endpoints to the real ones;

### Scalability
  - All the repository organization was done to create new tests easily as possible.
  - [Utils](https://gitlab.com/open-dish/datasci/-/blob/main/tests/tests_ml_component/utils.py) functions are generic enough to support many types of responses, including that ones that aren't in classic JSON format.
### Early Error Detection
  - In early tests, crucial errors like different response codes were detected and changes are made before final commit.

### Clean and Simple Test code

  - An example of a test file.

~~~python
import os
import unittest
from utils import get_test_file_name, validate_request

class TestPredict201(unittest.TestCase): #test in 201 case

    def __init__(self, *args, **kwargs):
        super(TestPredict201, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_{}.json".format(get_test_file_name(__file__))
        self.object_file = "./objects/predict_tabular_obj.json"
        self.endpoint = f"http://{os.getenv('TABULAR_HOST')}/predict"
        self.request_type = "POST"
        self.expected_code = 201
        self.metrics = ["response", "code"]
        

    def test_request(self):
        validate_request(self)

class TestPredict400(unittest.TestCase): #predict in 400 case

    def __init__(self, *args, **kwargs):
        super(TestPredict400, self).__init__(*args, **kwargs)

        # define variables
        self.schema_file = "./response_schemas/schema_none.json"
        self.object_file = "./objects/none_obj.json"
        self.endpoint = f"http://{os.getenv('TABULAR_HOST')}/predict"
        self.request_type = "POST"
        self.expected_code = 400
        self.metrics = ["code"]
        

    def test_request(self):
        validate_request(self)

~~~

### Versatile Validate Function
- Can test only response, code or both
- Works with non-ortodox JSON responses (Ex: that ones that starts without "{" )
- Support onnx files in mock objects

Here is a closer look into the validate_request function:

~~~python
def validate_request(self): #take request and validate it by the schemas
    obj = {}
    with open(self.schema_file) as file:
        data = json.load(file)
        schema = data
        validator = cerberus.Validator(schema)

        if (self.object_file == "./objects/model2.onnx"):
            res, code = make_request_onnx(self.endpoint, self.object_file)
        else:
            if self.request_type == 'POST':
                with open(self.object_file) as f:
                    obj = json.load(f)
            res, code = make_request(self.endpoint, self.request_type, obj)

        if ("response" in self.metrics):
            print("RESPONSE FROM API")
            print(res)
            val = validator.validate(res,schema)
            if validator.errors != {}:
                print("ERRORS")
                print(validator.errors)
            else:
                print("Schema OK!")
            self.assertEqual(val,True)
        if ("code" in self.metrics):
            self.assertEqual(code,self.expected_code)
    return

~~~
