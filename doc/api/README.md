# open-dish Data Models and APIs

This specification defines models in four categories:

* [Base Models](base.md)
* [Data Management Models](management.md)
* [Data Science Models](datasci.md)
* [Client Models](client.md)

They will follow Gitlab's [Documenting REST API resources](https://docs.gitlab.com/ee/development/documentation/restful_api_styleguide.html).

All models will consider a subset from a hospital infrastructure, in which the main problem is a hemogram exam and anemia detection/analysis.
