# Data Science API and Models

API related to data science services integrated into a hospital infrastructure. Expand the base models with data science concerns.

# API Specification

## API name

> Version history note.

One or two sentence description of what endpoint does.

### Method title

> Version history note.

Description of the method.

```plaintext
METHOD /endpoint
```

Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `attribute`              | datatype | Yes      | Detailed description. |
| `attribute` **(<tier>)** | datatype | No       | Detailed description. |
| `attribute`              | datatype | No       | Detailed description. |
| `attribute`              | datatype | No       | Detailed description. |

If successful, returns [`<status_code>`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `attribute`              | datatype | Detailed description. |
| `attribute` **(<tier>)** | datatype | Detailed description. |

Example request:

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/endpoint?parameters"
```

Example response:

```json
[
  {
  }
]
```