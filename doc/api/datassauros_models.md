# Persisted Data API and Models
# Support API

API for supporting the client and simulating a real operation.

# API Specification

## Sundae API

> Version 1.0.0

API using by the client to simulate the patient's entry into the system.

### Get Next Patient

> Version 1.0.0

Returns the next patient's id used to request his data into the bus.

```plaintext
GET /next-patient
```

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `Id`              | UUID | Unique Identifier of the patient. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/sundae/next-patient"

```

* Response:

```json
[
  {
    "id": "1d604da9-9a81-4ba9-80c2-de3375d59b40"
  }
]
```


# Base Models

Contain all persisted data. They are the starting point for all other models. Here they will be a subset of models expected in a hospital infrastructure focusing on the *patient* and *hemogram*.

# Models Specification

The models below are based on Synthea models. The ideas presented in openEHR are the best for persistent health data, but it is too complex for this project. Thus, by using the "Synthea models" it’s possible to simplify and at the same time allow the models to expand in the future. The models below aren't a copy of the Synthea documentation, a lot of tables and files were dropped because they wouldn’t be very useful for this project, at least for now.

## Patients

> Version 1.0.2.

Patient demographic data.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Primary Key. Unique Identifier of the patient. |
| `birthDate`              | Date (`YYYY-MM-DD`) | The date the patient was born. |
| `deathDate`              | Date (`YYYY-MM-DD`) | The date the patient died. |
| `first`              | String | First name of the patient. |
| `last`              | String | Last or surname of the patient. |
| `marital`              | String | Marital Status. `M` is married, `S` is single. Currently no support for divorce (`D`) or widowing (`W`) |
| `race`              | String | Description of the patient's primary race. |
| `ethnicity`              | String | Description of the patient's primary ethnicity. |
| `gender`              | String | Gender. `M` is male, `F` is female. |
| `birthPlace`              | String | Name of the town where the patient was born. |
| `lat`              | Numeric | Latitude of Patient's address. |
| `lon`              | Numeric | Longitude of Patient's address. |

### Example in JSON Format:

```json
[
  {
  "patient": {
    "id": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "birthDate": "1998-05-25",
    "deathDate": "",
    "first": "Mariana",
    "last": "Rutherford",
    "marital": "M",
    "race": "white",
    "ethnicity": "hispanic",
    "gender": "F",
    "birthPlace": "Yarmouth  Massachusetts  US",
    "lat": "42.63614335069588",
    "lon": "-71.3432549217789"
    }
  }
]
```
---

## Observations

> Version 1.0.2.

Patient observations including laboratory tests.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `date`              | Date	iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the observation was performed. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the observation was performed. |
| `category`              | String | Observation category. |
| `code`              | String | Observation or Lab code from LOINC |
| `description`              | String | Description of the observation or lab. |
| `value`              | String | The recorded value of the observation. |
| `units`              | String | The units of measure for the value. |
| `type`              | String | The datatype of `Value`: `text` or `numeric` |

### Example in JSON Format:

```json
[
  {
  "observation": {
    "date": "2012-01-23T17:45:28Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "category": "laboratory",
    "code": "718-7",
    "description": "Hemoglobin [Mass/volume] in Blood",
    "value": "15.1",
    "units": "g/dL",
    "type": "numeric"
    }
  }
]
```

---

## Encounters

> Version 1.1.0.

Patient encounter data.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Primary Key. Unique Identifier of the encounter. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter started. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter concluded. |
| `patient`              | UUID | Foreign key to the Patient. |
| `provider` | UUID | Unique Identifier of the clinicians that provide patient care. |
| `encounterClass`              | String | The class of the encounter, such as `ambulatory`, `emergency`, `inpatient`, `wellness`, or `urgentcare` |
| `code`              | String | Encounter code from SNOMED-CT |
| `description`              | String | Description of the type of encounter. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT, only if this encounter targeted a specific condition. |
| `reasonDescription`              | String | Description of the reason code. |

### Example in JSON Format:

```json
[
  {
  "encounter": {
    "id": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "start": "2010-01-23T17:45:28Z",
    "stop": "2010-01-23T18:10:28Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "provider": "bde99eb0-3ee8-30b7-8a03-d8b99c91a6b7",
    "encounterClass": "ambulatory",
    "code": "185345009",
    "description": "Encounter for symptom",
    "reasonCode": "10509002",
    "reasonDescription": "Acute bronchitis (disorder)"
    }
  }
]
```

---

## Conditions

> Version 1.0.2.

Patient conditions or diagnoses.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | Date (`YYYY-MM-DD`) | The date the condition was diagnosed. |
| `stop`              | Date (`YYYY-MM-DD`) | The date the condition resolved, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter when the condition was diagnosed. |
| `code`              | String | Diagnosis code from SNOMED-CT |
| `description`              | String | Description of the condition. |

### Example in JSON Format:

```json
[
  {
  "condition": {
    "start": "2010-03-17",
    "stop": "2010-04-16",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "36971009",
    "description": "Sinusitis (disorder)"
    }
  }
]
```

---

## Medications

> Version 1.0.2.

Patient medication data.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the medication was prescribed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the prescription ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the medication was prescribed. |
| `code`              | String | Medication code from RxNorm. |
| `description`              | String | Description of the medication. |
| `dispenses`              | Numeric | The number of times the prescription was filled. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this medication was prescribed. |
| `reasonDescription`              | String | Description of the reason code. |

### Example in JSON Format:

```json
[
  {
  "medication": {
    "start": "2020-11-25T12:12:33Z",
    "stop": "2020-12-30T21:12:49Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "562251",
    "description": "Amoxicillin 250 MG / Clavulanate 125 MG Oral Tablet",
    "dispenses": "1",
    "reasonCode": "36971009",
    "reasonDescription": "Sinusitis (disorder)"
    }
  }
]
```


---

## Allergies

> Version 1.0.2.

Patient allergy data.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | Date (`YYYY-MM-DD`) | The date the allergy was diagnosed. |
| `stop`              | Date (`YYYY-MM-DD`) | The date the allergy ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter when the allergy was diagnosed. |
| `code`              | String | Allergy code |
| `system`              | String | Terminology system of the Allergy code. `RxNorm` if this is a medication allergy, otherwise `SNOMED-CT`. |
| `description`              | String | Description of the `Allergy` |
| `type`              | String | Identify entry as an `allergy` or `intolerance`. |
| `category`              | String | Identify the category as `drug`, `medication`, `food`, or `environment`. |
| `reaction1`              | String | Optional SNOMED code of the patients reaction. |
| `description1`              | String | Optional description of the `Reaction1` SNOMED code. |
| `severity1`              | String | Severity of the reaction: `MILD`, `MODERATE`, or `SEVERE`. |
| `reaction2`              | String | Optional SNOMED code of the patients second reaction. |
| `description2`              | String | Optional description of the `Reaction2` SNOMED code. |
| `severity2`              | String | Severity of the second reaction: `MILD`, `MODERATE`, or `SEVERE`. |

### Example in JSON Format:

```json
[
  {
  "allergy": {
    "start": "2010-03-17",
    "stop": "",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "762952008",
    "system": "Unknown",
    "description": "Peanut (substance)",
    "type": "allergy",
    "category": "food",
    "reaction1": "247472004",
    "description1": "Wheal (finding)",
    "severity1": "MODERATE",
    "reaction2": "271807003",
    "description2	": "Eruption of skin (disorder)",
    "severity2": "MILD",
    }
  }
]
```

---

## Procedures

> Version 1.0.2.

Patient procedure data including surgeries.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the procedure was performed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the procedure was completed, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the procedure was performed. |
| `code`              | String | Procedure code from SNOMED-CT |
| `description`              | String | Description of the procedure. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this procedure was performed. |
| `reasonDescription`              | String | Description of the reason code. |

### Example in JSON Format:

```json
[
  {
  "procedure": {
    "start": "2020-11-25T21:12:49Z",
    "stop": "2020-11-25T21:23:25Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "261352009",
    "description": "Face mask (physical object)",
    "reasonCode": "840544004",
    "reasonDescription": "Suspected COVID-19"
    }
  }  
]
```

---

## Image

> Version 2.0.2.

Patient images references.

### Schema in Relational Format

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Unique identifier of the image. |
| `date`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the image was taken. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the image was taken. |
| `imagePath`              | String | Path to image in the database. |

### Example in JSON Format:

```json
[
  {
  "image": {
    "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
    "date": "2003-11-01T20:30:34Z",
    "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
    "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
    "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png"
    }
  }  
]
```
