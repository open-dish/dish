# Data Science API and Models

API related to data science services integrated into a hospital infrastructure. Expand the base models with data science concerns.


## Model serving/management API

> This API handles the management of models and model serving. It receives requests from the client and fowards into a specific model, it also saves the model using containers and the metadata about the models . It was agreed that the API would be implemented in a top down approach, where firstly we would have endpoints related to machine learning areas such as audio, image, text, etc. and then we would have endpoints related specifics tasks from each each area. For example, we would have an endpoint for audio classification and then we would have endpoints for specific tasks such as speech to text, speech to text translation, etc. For each specific area, we would have a set of endpoints for model management and model serving. The endpoints for model management would be related to the model versioning and model deployment. The endpoints for model serving would be related to the model inference. Using this approach, we would have common fields in more general areas and specific fields in more specific areas. This approach would allow us to have a more flexible API and also to have a more flexible model serving and management.



### There will be three main components in the API:

1. **Model Handler**: This component will be responsible for handling the model serving and management. It will receive requests from the client and fowards into a specific model. In a way that only this component will have direct access to the models, excluding direct contact from clients/third party with them. This design allows a easy way of adding new plugins, in this case models. 

2. **Model Saving/Versioning**: This component will be responsible for saving the models and managing the model versions. It will be responsible for saving the models in a specific location and also for managing the model versions. It will be responsible for creating a new version of a model and also for deleting a specific version of a model. It will also be responsible for creating a new model and also for deleting a specific model. The way we choose to save the models is using gitlab registry and with containers

3. **Model Metadata saving**: This component will be responsible for saving the metadata of the models, where he is stored, and the url, which will be used for the model handler to forward the request to the correct model. It will also be responsible for saving the metadata of the model versions, such as the model version, the model url, the model description, the input format, output format, etc. The way we choose to save the metadata is using a database, using SQLite.






## Model Handler

### Model Handler API

> This component is mono route, there will be only one route to direct the predict request into the specific model, receiving the data from the client and sending the result into the database manegament

#### Predict

> This method is used to get a prediction from a model in a task in an area.

```plaintext
POST /model-handler/areas/<area>/tasks/<task>/models/<model>/versions/<version>/predict
```

Supported attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| area                     | String   | Yes      | Name of the area     |
| task                     | String   | Yes      | Name of the task     |
| model                    | String   | Yes      | Name of the model   |
| version                  | String   | Yes      | Name of the version  |





There will be different requisitons for different areas and tasks, for example, for image classification, the request will be a json with the image path and for tabular classification, the request will be a json with the data. The response will be a json with the prediction and the probability of the prediction. The response will be saved in the database management.


### Tabular

Example request tabular:
```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/model-handler/areas/tabular/tasks/classification/models/decision-tree/versions/1.0.0/predict"
```

Example request body tabular:
```json
{
    "id": "42bfe9b8-3f5a-11ed-b878-0242ac120002",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "status": "final",
    "resultsInterpreter": "cf546bba-6fa5-430f-9656-300dda3cf865",
    "result": {
        "observation": [ { "category": "laboratory",
          "code": "718-7",
          "value": "15.1",
          "units": "g/dL",
          "type": "numeric"},],
    },
    "study":[
      "1d88a815-fbc2-9b61-b582-944100e41fd1"
    ],
    "conclusionCode": [
      2694001
    ]
    "realibility": [
      1
    ]
}
```

### Image

Example request image:
```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/model-handler/areas/image/tasks/classification/models/decision-tree/versions/1.0.0/predict"
```
Example request body image:
```json

"image": {
  "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
  "date": "2003-11-01T20:30:34Z",
  "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
  "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
  "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png"
  }

```

response attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| id                       | String   | Yes      | Id of the pacient     |



If successful, all routes returns [`200`](../../api/index.md#status-codes) and the following

Example response:
```json

  {
    "id": "42bfe9b8-3f5a-11ed-b878-0242ac120002"
  }

```

## Model Saver

> This component is responsible for storing and fetching models to and from the project's Container Registry

#### Upload model


```plaintext
POST /model-saver/upload-model/areas/{area}/tasks/{task}/models/{model}
```

Supported attributes (encoded as "multipart/form-data"):
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| area                     | String   | Yes | Existing area to add the model to |    
| task                     | String   | Yes | Existing task to add the model to |    
| model                    | String   | Yes | The model's name |    
| version                  | String   | Yes | The model's version |    
| description              | String   | Yes | The model's description |    
| input_format             | String   | Yes | The model's accepted input formats |    
| output_format            | String   | Yes | The model's accepted output formats |    
| url_production           | String   | Yes | URL to the model running in production|    
| model_file               | File     | Yes | A docker image containing the model as a tar file|    

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:  
| Attribute                | Type     | Description                      |  
|:-------------------------|:---------|:---------------------------------|  
| area                    | Array    | The model's area |    
| task                    | Array    | The model's task |    
| model                   | Array    | The model's name |    
| version                 | Array    | The model's version |    
| url_registry            | Array    | URL to the uploaded image on the registry |    

Example request:
```shell
curl -X 'POST' \
  'https://gitlab.example.com/model-saver/upload-model/areas/text/tasks/entity-recognition/models/sample_model' \
  -H 'accept: application/json' \
  -H 'Content-Type: multipart/form-data' \
  -F 'version=1.1.1' \
  -F 'description=a model' \
  -F 'input_format=files' \
  -F 'output_format=other files' \
  -F 'url_production=http://example.com' \
  -F 'model_file=@model.tar;type=application/x-tar'
```

Example response:
```json
{
  "area": "text",
  "task": "entity-recognition",
  "model": "sample_model",
  "version": "1.1.1",
  "url_registry": "registry.gitlab.com/open-dish/dish/sample_model:1.1.1"
}
```


#### Fetch model


```plaintext
GET /model-saver/fetch-model/{model}/{version}
```

Supported attributes (encoded as "multipart/form-data"):
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| model                    | String   | Yes | The model's name |    
| version                  | String   | Yes | The model's version |    

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:  
| Attribute                | Type     | Description                      |  
|:-------------------------|:---------|:---------------------------------|  
| model_file               | File     | A docker image containing the model as a tar file|    

Example request:
```shell
curl -X 'GET' \
  'http://localhost:8000/model-saver/fetch-model/sample_model/1.1.1' \
  -H 'accept: application/json'
```

Example response:
```plaintext
TAR file
```




## Model Metadata saving

> This component is responsible for saving metadata about the models, areas and tasks.

### Machine learning areas

> This section describes the endpoints related to machine learning areas, such as audio, image, text, etc. The endpoints for each area are described in the following subsections.

#### Get areas

```plaintext
GET /model-metadata/areas
```

Supported attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:  
| Attribute                | Type     | Description                      |  
|:-------------------------|:---------|:---------------------------------|  
| areas                    | Array    | List of areas that are supported by the API and their description |    

Example request:
```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/model-metadata/areas"
```

Example response:
```json
{
  "areas": [
    {
      "name": "audio",
      "description": "Audio related endpoints"
    },
    {
      "name": "image",
      "description": "Image related endpoints"
    },
    {
      "name": "text",
      "description": "Text related endpoints"
    }
  ]
}
```

#### Insert a new area

> This method is used to insert a new area.

```plaintext
POST /model-metadata/areas
```

Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| area                    | String   | Yes      | Name of the area      |
| description              | String   | Yes      | Description of the area |

Example request:
```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/model-metadata/areas"
```



Example request body:
```json
{
  "name": "tabular",
  "description": "Tabular related endpoints"
}
```

If successful, returns [`200`](../../api/index.md#status-codes) and the following

Example response:
```json
{
  "area": "tabular",
}
```



### Machine learning tasks

> This section describes the endpoint of a specific machine learning area and its tasks. The endpoint for each task is described in the following subsections.

#### Get tasks

```plaintext
GET /model-metadata/areas/<area>
```

Supported attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| area                     | String   | Yes      | Name of the area     |

If successful, returns [`200`](../../api/index.md#status-codes) and the following

response attributes:      
| Attribute                | Type     | Description                                  |    
|:-------------------------|:---------|:---------------------------------------------|  
| area                     | String   | Name of the area.                            |    
| avaliable-tasks         | Array    | List of tasks that are supported by the API |    

Example request:
```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/model-metadata/areas/audio"
```





Example response:
```json
[
  {
    "area": "audio",
    "available-tasks": [
      "speech to text",
      "speech to text translation"
    ]
  }
]
```



#### Get task info

> This method retuns the description of an avaliable task, including its input and output formats. This method is used by the client to understand the input and output format of a task.



```plaintext
GET /model-metadata/areas/<area>/<task>
```

supported attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| area                     | String   | Yes      | Name of the area  |
| task                     | String   | Yes      | Name of the task   |

If successful, returns [`200`](../../api/index.md#status-codes) and the following

response attributes:
| Attribute                | Type     | Description                                  |
|:-------------------------|:---------|:---------------------------------------------|
| area                     | String   | Name of the area                            |
| task                     | String   | Name of the task                           |
| description              | String   | Description of the task                    |




example request:
```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/model-metadta/areas/audio/speech-to-text"
```


example response:
```json
[
  {
    "area": "audio",
    "task": "speech to text",
    "description": "This task converts speech to text.",
  }
]
```

### Insert task

> If a area is already added, this method is used to add a new task into the area.

```plaintext
POST /model-metadata/areas/<area>/tasks/<task>
```

Supported attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| area                     | String   | Yes      | Name of the area     |
| task                     | String   | Yes      | Name of the task     |



Example request:
```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/model-metadata/areas/text/tasks/name-entity-recognition"
```
Example request body:
```json
{
  "description": "This task extracts the name entities from a text.",
}
```

If successful, returns [`201`](../../api/index.md#status-codes) and the following

response attributes:
| Attribute                | Type     | Description                                  |
|:-------------------------|:---------|:----------------------|
| area                     | String   | Name of the area.                            |
| task                     | String   | Name of the task.                            |

Example response:
```json
[
  {
    "area": "text",
    "task": "name entity recognition"
  }
]
```




### Machine learning models

> This section describes the endpoints related to models in a specific task. The endpoints for each model are described in the following subsections.

#### Get models

```plaintext
GET /model-metadata/areas/<area>/<task>/models
```

Supported attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| area                     | String   | Yes      | Name of the area     |
| task                     | String   | Yes      | Name of the task    |


If successful, returns [`200`](../../api/index.md#status-codes) and the following


Example request:
```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/model-metadata/areas/audio/speech-to-text/models"
```

Response attributes:
| Attribute                | Type     | Description                                 |
|:-------------------------|:---------|:----------------------|
| area                     | String   | Name of the area                            |
| task                     | String   | Name of the task                            |
| model                    | String   | Name of the model                           |
| version                  | String   | Version of the model                        |
| description              | String   | Description of the model                    |



Response is a list of models in the task. Each model is described by the following attributes:

Example response:
```json
[
  {
    "area": "audio",
    "task": "speech to text",
    "model": "model1",
    "version": "1.0",
    "description": "This task converts speech to text."
  }
]
```

  

#### Get model 

```plaintext
GET /model-metadata/areas/<area>/tasks/<task>/models/<model>/<version>
```

Supported attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| area                     | String   | Yes      | Name of the area     |
| task                     | String   | Yes      | Name of the task     |
| model                    | String   | Yes      | Name of the model    |
| version                  | String   | Yes      | The model's version  |

Example request:
```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/model-metadata/areas/audio/speech-to-text/models/model1"
```

If successful, returns [`200`](../../api/index.md#status-codes) and the following

response attributes:

| Attribute                | Type     | Description                                  |
|:-------------------------|:---------|:----------------------|
| area                     | String   | Name of the area.                            |
| task                     | String   | Name of the task.                            |
| model                    | String   | Name of the model.                           |
| version                  | String   | Version of the model.                        |
| description              | String   | Description of the model.                    |
| input_format             | String   | Input format of the model.                   |
| output_format            | String   | Output format of the model.                  |


Example response:
```json
[
  {
    "area": "audio",
    "task": "speech to text",
    "model": "model1",
    "version": "1.0",
    "description": "This task converts speech to text.",
    "input_format": "audio",
    "output_format": "text",
  }
]
```

