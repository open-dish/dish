# Introduction

In order to use this ML pipeline, a docker image must be sent to be integrated to the system, as some sort of plugin. The image, representing a new ML model, is responsible for define and implement caracteristic algorithms such as training, testing and predicting. Further details will be given as this document follows.
<br/>

# Plugin API and Models

Specification of API and models required to plugins (ML models)in order to be integrated to this component.

## Plugin Configuration Model

> Version 1.1.0

Specification of configuration file to add new ML model into the pipeline.

### <span style="color:GoldenRod">**Configuration**</span> Object Schema (follows FHIR standards):

```json
{
    "model-name": <string>,
    "container-image-path": <string>,
    "pre-trained": <boolean>,
    "data-type": <string>,
    "data": {
        "observation": {
            "laboratory-results": [
                "hemoglobin",
                "glucose",
                "erythrocyte",
                "hemoglobin",
                "base-excess",
                "CO2",
                "creatinine",
                "estimated gfr",
                "staphylococcus",
            ],
            "vital-signs": [
                "temperature",
                "weight",
                "respiratory-rate",
                "heart-rate",
                "body-temperature",
                "body-height",
                "body-length",
                "head-circumference",
            ]
        },
        "condition": [
            "fever",
            "malignancy",
            "sepsis",
            "renal-insufficiency",
            "bacterial-infection",
            "heart",
            "lung",
            "abscess",
        ]
    },
    "triggers": [
        {
            "type": <string>,
            "value": <number>,
            "fields": [
                "accuracy",
                "confusion-matrix",
                "precision-and-recall",
                "f1-score",
                "au-roc",
                "mae", //Mean Absolute Error
                "mse", //Mean Squared Error
                "rmse", //Root Mean Squared Error
                "r-squared"
            ]
        }
    ],
    "hyperparameters': {
        "key": default-value
    }

}
```

<br>

Supported attributes for *Configuration* Object:

| Attribute  | Required | Description  |
| :--------- | :------- | :----------- |
| `data`     | Yes      | Observation and condition are both standarized data by FHIR, aswell as laboratory-results and vital-signs inside observation. |
| `data-type`     | Yes      | Defines type of data input, for now we support `image` and `tabular`. |
| `container-image-path` | Yes      | Path to container image where ML Model APIs - as described before - are defined.        |
| `hyperparameters` | No     | Default values of hyperparameters used in model training.            || `pre-trained` | Yes      | Indicates wheter the ML Model already has weights to be used for training.            |
| `triggers` | No      | Mecanism to activate training step in pipeline.            |

<br>

At least one of the keys of data are required, but multiple are allowed.

For all of the arrays presented in the schema above, if your configuration json constains the key, for instance "vital-signs", then it is necessary that the array
contains at least one of the items (thus it should not be empty) and none of the items must repeat.

<br>

Supported attributes for *Trigger* Object:

| Attribute | Type     | Required                          | Description                                           |
| :-------- | :-----   | :-------------------------------- | :---------------------------------------------------- |
| `type`    | String   | Yes                               | Defines `time`-, `data`- or `metrics`-driven trigger. |
| `value`   | Number   | Only for `time` and `data` types. | Value to activate trigger (in hours for `time` and number of new registries for  `data`). |
| `fields`  | String[] | Only for `metrics` Type.          | List of metrics in Production to be compared in order to activate trigger.|

<br>


### <span style="color:GoldenRod">**Configuration**</span> Object Example:

```json
{
    "model-name": "MBMAM - My Beatiful Machine Learning Anemia Model",
    "image-path": "ml-models/this-model",
    "pre-trained": true,
    "data-type": "tabular",
    "data": {
        "observation": {
            "laboratory-results": ["hemogoblin"],
            "vital-signs": ["weight", "respiratory-rate", "heart-rate"]
        },
        "condition": ["heart"]
    },
    "triggers": [
        {
            "type": "time",
            "value": 240 //value in hours
        }
    ],
    "hyperparameters": {
        "learning-rate": 0.7,
        "number-hidden-layers": 3,
        "batch-size": 16
    }
}
```
<br />

## Training API Specification
API to be consumed by ML pipeline for training and testing specific ML model when requested.

### Training

> v1.1.0

This post is responsible to start the Machine Learn training.
The dev have  to implement this post request with this requeriments and usng this we can trigger a train.


```plaintext
POST /train
```
Supported attributes:

| Attribute  | Required | Type        | Description  |
| :--------- | :------- | :-----   | :----------- |
| `weights`     | Yes     | String | Address for the weight values of the pre-trained model. Is .pth or onnx file |
| `data` | Yes      | Object | Have the data to train the model. Each model will have its specific training data that can be image, tabular, etc|
| `hyperparameters` | Yes      | Dict | Hyperparameters required by ML model for training. Is a .json file|

Example of POST endpoint using Python and Flask

```python
@app.route('/train', methods=['POST'])
def train():
    if request.method == 'POST':
        file = request.files['file']
        data = resquest.data['data']
        hyperparameters = resquest.data['hyperparameters']
        ##############
        ##############
```
### Train Response

Attribute  | Type        | Description  |
| :--------- | :------- | :----------- |
| `weights_address`   | String | Address of the new trained weights |



### Testing

> v1.1.0

Description of the method.

```plaintext
POST /test
```

Supported attributes:
| Attribute  | Required | Type        | Description  |
| :--------- | :------- | :-----      | :----------- |
| `weights`     | Yes     | String | Address for the weight values of the model to test. Is .pth or onnx file |
| `data` | Yes      | Object | Have the data to test the model. Each model will have its specific data, can be image,tabular, etc|

Example of POST endpoint using Python and Flask

```python
@app.route('/test', methods=['POST'])
def test():
    if request.method == 'POST':
        file = request.files['file']
        data = resquest.data['data']
        ##############
        ##############
```
### Test Response

 Attribute   | Type        | Description  |
| :--------- | :------- | :----------- |
| `metrics`  | Dict | Performance metrics of the tested ML model according to the definition in the config file |

```json
{

    "data": {
        "output": {}
    },
     "metrics": {
        "accuracy": <number>,
        "confusion-matrix": <number>,
        "precision-and-recall": <number>,
        "f1-score": <number>,
        "au-roc": <number>,
        "mae": <number>, // Mean Absolute Error
        "mse": <number>, // Mean Squared Error
        "rmse": <number>, // Root Mean Squared Error
        "r-squared": <number>, // R²
    }
}
```
Refer to the "Standard data input and prediction formats" section for a better description of the prediction input and output formats for each ML service.


<br />

## Serving API Specification
API to be consumed by ML pipeline for serving predicition to client side.

### Prediction

> v1.1.0

```plaintext
POST /predict
```

Supported attributes:

| Attribute  | Required | Type        | Description  |
| :--------- | :------- | :-----      |:----------- |
| `data`     | Yes      | Object      |The input to make prediction, changes depending of the data model.        |


If successful, returns [`<200>`](../../api/index.md#200) and the following
response attributes:

| Attribute                | Type     | Description                                                       |
|:-------------------------|:---------|:----------------------                                            |
| `output`                   | Dict   | Returns the output of the prediction                              |

Example request:

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/predict?parameters"
```

Example response:

```json
{
    "output": [
        {
        "label": "Plateletes",
        "confidence": 0.131231231231,
        "topleft": {
            "x": 0,
            "y": 0
        },
        "bottomright": {
            "x": 33,
            "y": 35
        }
        },
        {
        "label": "RBC",
        "confidence": 0.543454534343,
        "topleft": {
            "x": 0,
            "y": 0
        },
        "bottomright": {
            "x": 71,
            "y": 86
        }
        }
    ]
}
```
Refer to the "Standard data input and prediction formats" section for a better description of the prediction input and output formats for each ML service.

### Update Prod Model

> v1.1.0

Description of the method.

```plaintext
POST /update-prod-model
```
Supported attributes:

| Attribute  | Required | Type        | Description  |
| :--------- | :------- | :-----      |:----------- |
| `model`    | Yes      | Bytes        |The model.(Pre-trained model). Is .pth or onnx file |

If successful, returns [`<200>`](../../api/index.md#200)

Response:

| Attribute                | Type     | Description                                                       |
|:-------------------------|:---------|:----------------------                                            |
| `message`                   | String   | A message regarding the successfulness of the updation      |

Example request:

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/update-prod-model?model-path=https%3A%2F%2Fmlmodels.com%2Fanemia_model.onnx%2F"
```

<br />

# ML Component API, Topics and Models

API, topics and models specification defined by ML Component in order to connect the steps needed to complete the pipeline flow.

<br>

## Evaluation API Specification
API to be consumed by plugins once they finish their training step, in order to continue the pipeline flow.

### Evaluate

> v1.1.0

Description of the method.

```plaintext
POST /evaluate
```
<br/>

Supported attributes:
| Attribute  | Required | Description  |
| :--------- | :------- | :----------- |
| `metrics` | Yes      | Performance metrics of the tested ML model |
| `other-metrics`     | Yes     | Performance metrics of model(s) in repository |
| `policy` | Yes      | Conditions on which the production model will be replaced by tested model. Is String. See below. |

If successful, returns [`<200>`](../../api/index.md#200) and the following
response attributes:

| Attribute                | Type     | Description                                                       |
|:-------------------------|:---------|:----------------------                                            |
| `is-better`                 | Boolean  | Tested ML model is better than current production model           |


Policy attribute:

Will be parsed according to the following regex:

    ((metric_1|metric_2|...|metric_n):(avg|median|75percentile|top);)+

Which can be interpreted as follows:

| Attribute                | Description                                                       |
|:-------------------------|:----------------------                                            |
| `metric_x`                 | One of *n* metrics in `json[metrics]`, except for `confusion-matrix` |                                         |
| `avg`                 | Select model if its `metric_x` is higher than *average* |   
| `median`                 | Select model if its `metric_x` is higher than the *median* |    
| `75percentile`                 | Select model if its `metric_x` is higher than 75% of models' |    
| `top`                 | Select model if its `metric_x` is the highest |    

Example:

    accuracy:median;f1-score:75percentile;au-roc:top;

Only sets API output `is-better` `True` if the testing models' accuracy is higher than median, its f1-score is within the 75th percentile and has the highest au-roc score.

## Trigger API Specification
API that allows a manual trigger for training ML models incorporated in this component.

### Activate

> v1.1.0

This method initiates the training process to a specified ML model.

```plaintext
POST /trigger/{model-id}
```
<br/>

Supported attributes:
| Attribute  | Required | Description  |
| :--------- | :------- | :----------- |
| `model-id` | Yes      | UUID that represents the ML model to be trained. |

If successful, returns [`<200>`](../../api/index.md#200).

## Statistics Model

> Version 1.1.0

Specification of statistics model that will be generated for each ML model after evaluation.

### <span style="color:GoldenRod">**Model Statistics**</span> Object Schema:

```json
{
    "id": <uuid>,
    "model-name": <string>,
    "model-id": <uuid>,
    "model-git-path": <string>,

    "metrics": {
        "accuracy": <number>,
        "confusion-matrix": <number>,
        "precision-and-recall": <number>,
        "f1-score": <number>,
        "au-roc": <number>,
        "mae": <number>, // Mean Absolute Error
        "mse": <number>, // Mean Squared Error
        "rmse": <number>, // Root Mean Squared Error
        "r-squared": <number>, // R²
    }
}
```

<br/>

## Standard data input and prediction formats
> Version 1.1.0

Every problem has a lot of different formats for data annotation, input and prediction. In order to guarantee interoperability, we focus on a limited set of data format for each kind of ML problem. In the future we aim to support more kinds of data.

Each kind of image input (image, tabular, text, etc.) has a standard format and each kind of problem (classification, object detector, NER, etc.) within each kind of input has a standard format for annotation (input for training) and prediction (output for inferencing).

**Data object schema:**

<br>

```json
{
    "type": <string>,
    "data": {
        "input": {},
        "annotation": {},
        "output": {}
    },
}
```
<br>

Supported attributes for *Data* Object:

| Attribute  | Required | Description  |
| :--------- | :------- | :----------- |
| `type`     | Yes      | Defines type of data input, for now we support `image` and `tabular`. |
| `data`     | Yes      | The input, annotations and outputs, in a way that this format can be used both for input and output.        |

<br>

If the "annotation" key of data exists, then "input" should also exists. If the "output" key of data exists, then no other key is allowed.

**Data Object** Example for anemia model:

```json
{
    "type": "tabular",
    "data": {
        "input":{
            "columns":[
                {
                    "name": "hemoglobin",
                    "type": "float",
                    "possible_values": None
                },
                {
                    "name": "weight",
                    "type": "float",
                    "possible_values": None
                },
                {
                    "name": "respiratory-rate",
                    "type": "float",
                    "possible_values": None
                },
                {
                    "name": "heart-rate",
                    "type": "float",
                    "possible_values": None
                },
                {
                    "name": "gender",
                    "type": "string",
                    "possible_values": ["woman", "man", "other"]
                }
            ],
            "values":[
                {
                    "hemoglobin": 100,
                    "weight": 75,
                    "respiratory-rate":10,
                    "heart-rate":5,
                    "gender": "man"
                },
                {
                    "hemoglobin": 120,
                    "weight": 71,
                    "respiratory-rate":5,
                    "heart-rate":2,
                    "gender": "other"
                },
                {
                    "hemoglobin": 50,
                    "weight": 40,
                    "respiratory-rate":2,
                    "heart-rate":2,
                    "gender": "woman"
                }
        },
        "annotation":{
            "name": "anemia",
            "type": "bool",
            "possible_values": None,
            "values":[
                True,
                True,
                False
            ]
        }
        
    }
}
```

<br>

### Image Classification

For this task, inputs, annotations and outputs are all lists of strings, in the case of annotations for each input, the order in which the images are sent should match the annotations.

**Input** Object Schema for Image Classification  tasks:

Since we are using REST APIs for communication and jsons, images are encoded as base64 strings.


```json
{
    [
        <base64string>,
        <base64string>,
        ...
    ]
}
```
<br>

**Annotation** Object Schema for Image Classification tasks

```json
{
    [
        <string>,
        <string>,
        ...
    ]
}
```

<br>

**Output** Object Schema for Image Classification tasks

```json
{
    "output": [{
        "label": "Platelets",
        "confidence": 0.28805842995643616,
        "topleft": {
            "x": 0,
            "y": 0
        },
        "bottomright": {
            "x": 33,
            "y": 35
        }
        },
        {
        "label": "RBC",
        "confidence": 0.28805845975875854,
        "topleft": {
            "x": 0,
            "y": 0
        },
        "bottomright": {
            "x": 71,
            "y": 87
        }
        },
        {
        "label": "RBC",
        "confidence": 0.28805845975875854,
        "topleft": {
            "x": 0,
            "y": 0
        }
        }
    ]
}
```

**Image Classification Object Example** for a binary classification problem:

~~~json
{
  "image": {
    "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
    "date":"2003-11-01T20:30:34Z",
    "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
    "encounter": "13efcb79-c259-e4a5-2258-ea655 ceeeb48",
    "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png"
  }
}
~~~

### Tabular (Classification and Regression)

**Input** Object Schema for Tabular data:

~~~json
{
   "patient":{
      "id": <string>,
      "birthDate": <date>,
      "deathDate": <date>,
      "first": <string>,
      "last": <string>,
      "race": <string>,
      "ethnicity": <string>,
      "gender": <string>,
      "birthPlace": <string>,
      "lat": <string>,
      "lon": <string>
   },
   "diagnostic":{
      "patient": <string>,
      "encounter": <string>,
      "result":{
         "observation":[
            {
               "category": <string>,
               "code": <string>,
               "value": <string>,
               "units": <string>,
               "type": <string>
            }
         ]
      },
      "resultsInterpreter": <string>,
      "status": <string>,
      "id": <string>,
   }
}
~~~
<br>

Supported attributes for *Input* Object:

| Attribute  | Required | Description  |
| :--------- | :------- | :----------- |
| `columns`     | Yes      | Describe all the columns of the table |
| `values` | Yes      | The table values            |

Supported attributes for *columns* Object:

| Attribute | Type     | Required                          | Description                                           |
| :-------- | :-----   | :-------------------------------- | :---------------------------------------------------- |
| `name`    | String   | Yes                               | Name of the column. |
| `type`   | String   | Yes | Type of the values of the column (float, int, string, etc). |
| `possible_values`  | [] | No          | List of possible values the column can assume.|

**Annotation** Object Schema for Tabular data:

```json
{
    "name": <string>,
    "type": <string>,
    "possible_values": []
    "values":[
        "value-1",
        "value-2",
        ...
    ]
}
```

Supported attributes for *annotation* Object:

| Attribute | Type     | Required                          | Description                                           |
| :-------- | :-----   | :-------------------------------- | :---------------------------------------------------- |
| `name`    | String   | Yes                               | Name of the column. |
| `type`   | String   | Yes | Type of the values of the column (float, int, string, etc). |
| `possible_values`  | [] | No          | List of possible values the column can assume.|
| `values`  | [] | Yes          | The values itself.|

<br>

**Output** Object Schema for Tabular data

```json
{
    [
        "value-1",
        "value-2",
        ...
    ]
}
```

**Tabular Data Object Example** for Input of Anemia Model :

~~~json
{
   "patient":{
      "id":"4b8ae268-a30f-76a4-c560-66965dc7dc4f",
      "birthDate":"1970-06-26",
      "deathDate":"nan",
      "first":"Juliane",
      "last":"Ledner",
      "race":"white",
      "ethnicity":"nonhispanic",
      "gender":"M",
      "birthPlace":"Norwood  Massachusetts  US",
      "lat":"42.56172224791635",
      "lon":"-71.08529432514787"
   },
   "diagnostic":{
      "patient":"4b8ae268-a30f-76a4-c560-66965dc7dc4f",
      "encounter":"2463ab8c-120f-cd69-0480-8f785199a26e",
      "result":{
         "observation":[
            {
               "category":"survey",
               "code":"718-7",
               "value":"9.0",
               "units":"nan",
               "type":"text"
            },
            {
               "category":"survey",
               "code":"785-6",
               "value":"21.5",
               "units":"{score}",
               "type":"numeric"
            },
            {
               "category":"survey",
               "code":"786-4",
               "value":"29.6",
               "units":"nan",
               "type":"text"
            },
            {
               "category":"survey",
               "code":"787-2",
               "value":"71.2",
               "units":"{score}",
               "type":"numeric"
            }
         ]
      },
      "resultsInterpreter":"d280c09b-eec6-3c56-bdfe-8ae98404e4e6",
      "status":"final",
      "id":"589291b1-7e3d-9158-283d-015c7b0f445f",
   }
}
~~~



## Topics Specification

### <b>Data Monitoring Topic</b>
> Version 1.1.0

Specification of topic to control data inserting/updating in order to check for triggers activation.

Supported attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `type`              | String | Yes      | Type of resource modified (e.g.: `observation`, `condition`). |
| `code` | String | Yes       | Code that specifies the type of data modified. |
| `description`              | String | No       | Type description of data modified. |
| `date`              | Date | Yes       | Date of the ocurring event. |


Data monitoring object example:
```json
{
	"observation": {
		"code": "718-7",
		"description": "Hemoglobin [Mass/volume] in Blood",
        "date": "2012-04-23T18:25:43.511Z"
	}
}
```

### <b>Model Monitoring Topic</b>
> Version 1.1.0

Specification of topic to control ML model metrics in Production in order to check for triggers activation.

Supported attributes:
| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `model-id`              | UUID | Yes      | ID of model that generated the metrics. |
| `metrics` | Object | Yes       | Same object specified in "Model Statistics Object Schema" containing the generated metrics. |


Data monitoring object example:
```json
{
    "model-id": <UUID>,
	"metrics": {
        "accuracy": <number>,
        "confusion-matrix": <number>,
        "precision-and-recall": <number>,
        "f1-score": <number>,
        "au-roc": <number>,
        "mae": <number>,
        "mse": <number>,
        "rmse": <number>,
        "r-squared": <number>
    }
}
```
