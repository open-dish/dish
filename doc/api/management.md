# Data Management API and Models

API that reflects the access and maintenance of data in the base models.

## Sections:

* [List all Patients](#list-all-patients)
* [Get Patient by ID](#get-patient-by-id)
* [Post Patient](#post-patient)
* [Put Patient](#put-patient)
* [Delete Patient](#delete-patient)
* [List all Encounters](#list-all-encounters)
* [Get Encounter by ID](#get-encounter-by-id)
* [Get Encounter by patient ID](#get-encounter-by-patient-id)
* [Post Encounter](#post-encounter)
* [Put Encounter](#put-encounter)
* [Delete Encounter](#delete-encounter)
* [List all Diagnostic Reports](#list-all-diagnostic-reports)
* [Get Diagnostic Report](#get-diagnostic-reports)
* [Post Diagnostic Report](#post-diagnostic-report)
* [Put Diagnostic Report](#put-diagnostic-report)
* [List all Images](#list-all-images)
* [Get Images](#get-images)
* [Post Images](#post-images)
* [Put Image](#put-image)
* [Delete Image](#delete-image)
* [List all Medications](#list-all-medications)
* [Get Medications](#get-medications)
* [Post Medications](#post-medications)
* [Put Medication](#put-medication)
* [Delete Medication](#delete-medication)
* [List all Procedures](#list-all-procedures)
* [Get Procedures](#get-procedures)
* [Post Procedures](#post-procedures)
* [Put Procedure](#put-procedure)
* [Delete Procedure](#delete-procedure)
* [List all Allergies](#list-all-allergies)
* [Get Allergies](#get-allergies)
* [Post Allergies](#post-allergies)
* [Put Allergy](#put-allergy)
* [Delete Allergy](#delete-allergy)
---

# API Specification

## Suco de API

> Version 4.2.1

Final version

### List all Patients

* [Return](#sections)

> Version 4.2.1

Returns a list of all or patients with their full information in the database separated in pages

```plaintext
GET /patient/
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `pageNumber`  | integer | No | Number of the page you need. |
| `nPatient`  | integer | No | Number of patients per page. |


If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the patient. |
| `birthDate`              | Date (`YYYY-MM-DD`) | The date the patient was born. |
| `deathDate`              | Date (`YYYY-MM-DD`) | The date the patient died. |
| `first`              | String | First name of the patient. |
| `last`              | String | Last or surname of the patient. |
| `race`              | String | Description of the patient's primary race. |
| `ethnicity`              | String | Description of the patient's primary ethnicity. |
| `gender`              | String | Gender. `M` is male, `F` is female. |
| `birthPlace`              | String | Name of the town where the patient was born. |
| `lat`              | Numeric | Latitude of Patient's address. |
| `lon`              | Numeric | Longitude of Patient's address. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/patient"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "id": "d81d9539-306b-a45d-9a7f-c0962e1719e0",
    "birthDate": "2017-08-11",
    "deathDate": "nan",
    "first": "Cortney",
    "last": "Morar",
    "race": "white",
    "ethnicity": "nonhispanic",
    "gender": "F",
    "birthPlace": "Holliston  Massachusetts  US",
    "lat": "42.268643930057024",
    "lon": "-71.4457107541436"
  },
  {
    "_id": "6386cb74921e2b2ff211c260",
    "id": "40ef7d80-60b0-ee5c-16e5-c8d6b1e579f1",
    "birthDate": "2014-07-03",
    "deathDate": "nan",
    "first": "Curt",
    "last": "Cormier",
    "race": "white",
    "ethnicity": "nonhispanic",
    "gender": "M",
    "birthPlace": "North Andover  Massachusetts  US",
    "lat": "42.67037685989471",
    "lon": "-71.18719983679158"
  },
  ...
]
```

---

### Get Patient by ID

* [Return](#sections)

> Version 4.2.1

Returns patient informations by ID

```plaintext
GET /patient/{id}
```

* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`  | UUID | Yes | Primary Key. Unique Identifier of the patient. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the patient. |
| `birthDate`              | Date (`YYYY-MM-DD`) | The date the patient was born. |
| `deathDate`              | Date (`YYYY-MM-DD`) | The date the patient died. |
| `first`              | String | First name of the patient. |
| `last`              | String | Last or surname of the patient. |
| `race`              | String | Description of the patient's primary race. |
| `ethnicity`              | String | Description of the patient's primary ethnicity. |
| `gender`              | String | Gender. `M` is male, `F` is female. |
| `birthPlace`              | String | Name of the town where the patient was born. |
| `lat`              | Numeric | Latitude of Patient's address. |
| `lon`              | Numeric | Longitude of Patient's address. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/patient?Patient_ID"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "id": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "birthDate": "1998-05-25",
    "deathDate": "",
    "first": "Mariana",
    "last": "Rutherford",
    "race": "white",
    "ethnicity": "hispanic",
    "gender": "F",
    "birthPlace": "Yarmouth  Massachusetts  US"
    "lat": "42.63614335069588",
    "lon": "-71.3432549217789"
  }
]
```

---

### Post Patient

* [Return](#sections)

> Version 2.2.1

Post patient data into the database

```plaintext
POST /patient
```

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Primary Key. Unique Identifier of the patient. |
| `birthDate`              | Date (`YYYY-MM-DD`) | The date the patient was born. |
| `deathDate`              | Date (`YYYY-MM-DD`) | The date the patient died. |
| `first`              | String | First name of the patient. |
| `last`              | String | Last or surname of the patient. |
| `race`              | String | Description of the patient's primary race. |
| `ethnicity`              | String | Description of the patient's primary ethnicity. |
| `gender`              | String | Gender. `M` is male, `F` is female. |
| `birthPlace`              | String | Name of the town where the patient was born. |
| `lat`              | Numeric | Latitude of Patient's address. |
| `lon`              | Numeric | Longitude of Patient's address. |

```json
[
  {
  "patient": {
    "id": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "birthDate": "1998-05-25",
    "deathDate": "",
    "first": "Mariana",
    "last": "Rutherford",
    "race": "white",
    "ethnicity": "hispanic",
    "gender": "F",
    "birthPlace": "Yarmouth  Massachusetts  US"
    "lat": "42.63614335069588",
    "lon": "-71.3432549217789"
    }
  }
]
```
If successful, returns [`200`](../../api/index.md#status-codes)

* Request:

```shell
curl -X POST -H "PRIVATE-TOKEN: <your_access_token>" -d @path/to/file.json "https://gitlab.example.com/api/suco/patient"
```

---

### Put Patient

* [Return](#sections)

> Version 4.2.1

Updates patient with the request body passed by the user

```plaintext
PUT /patient/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the patient. |

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the patient. |
| `birthDate`              | Date (`YYYY-MM-DD`) | The date the patient was born. |
| `deathDate`              | Date (`YYYY-MM-DD`) | The date the patient died. |
| `first`              | String | First name of the patient. |
| `last`              | String | Last or surname of the patient. |
| `race`              | String | Description of the patient's primary race. |
| `ethnicity`              | String | Description of the patient's primary ethnicity. |
| `gender`              | String | Gender. `M` is male, `F` is female. |
| `birthPlace`              | String | Name of the town where the patient was born. |
| `lat`              | Numeric | Latitude of Patient's address. |
| `lon`              | Numeric | Longitude of Patient's address. |

```json
[
  {
    "id": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "birthDate": "1998-05-25",
    "deathDate": "",
    "first": "Mariana",
    "last": "Rutherford",
    "race": "white",
    "ethnicity": "hispanic",
    "gender": "F",
    "birthPlace": "Yarmouth  Massachusetts  US"
    "lat": "42.63614335069588",
    "lon": "-71.3432549217789"
  }
]
```

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X PUT -H "PRIVATE-TOKEN: <your_access_token>" -d @json_file "https://gitlab.example.com/api/suco/patient?id"
```

---

### Delete Patient

* [Return](#sections)

> Version 4.2.1

Updates encounter with the request body passed by the user

```plaintext
DELETE /patient/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the patient. |

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X DELETE -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/patient?id"
```

---


### List all Encounters

* [Return](#sections)

> Version 4.2.1

Returns all the encounters in the database

```plaintext
GET /encounter/
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `pageNumber`  | integer | No | Number of the page you need. |
| `nEncounter`  | integer | No | Number of patients per page. |
If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the encounter. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter started. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter concluded. |
| `patient`              | UUID | Foreign key to the Patient. |
| `participantactor`    | UUID | ID of the medic responsible the encounter. |
| `encounterClass`              | String | The class of the encounter, such as `ambulatory`, `emergency`, `inpatient`, `wellness`, or `urgentcare` |
| `code`              | String | Encounter code from SNOMED-CT |
| `description`              | String | Description of the type of encounter. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT, only if this encounter targeted a specific condition. |
| `reasonDescription`              | String | Description of the reason code. |


* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/encouter?Patient"
```

* Response:

```json
[
  {
    "_id": "63741993851aada93ab6f3cc",
    "id": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "start": "2010-01-23T17:45:28Z",
    "stop": "2010-01-23T18:10:28Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "participantActor": "1dfa9e64-9af1-497d-be21-52be529fdbc3",
    "encounterClass": "ambulatory",
    "code": "185345009",
    "description": "regular check-up",
    "reasonCode": "10509002",
    "reasonDescription": "Acute bronchitis"
  },
  {
    "_id": "6375704830da42640a8324ac",
    "id": "4f0d92bb-a71b-26af-7edc-cb9cb0923c32",
    "start": "2021-09-05T10:27:33Z",
    "stop": "2021-09-05T10:42:33Z",
    "patient": "6489e75e-beac-832d-e053-3905d1fee0f6",
    "participantActor": "8c1d5033-e188-3d65-8266-3d1c86724ee2",
    "encounterClass": "wellness",
    "code": "410620009",
    "description": "Well child visit (procedure)",
    "reasonCode": "nan",
    "reasonDescription": "nan"
  },
  ...
]
```

---

### Get Encounter by ID

* [Return](#sections)

> Version 4.2.1

Returns encounter informations by ID

```plaintext
GET /encounter/{Id}
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the encounter. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the encounter. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter started. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter concluded. |
| `patient`              | UUID | Foreign key to the Patient. |
| `participantactor`    | UUID | ID of the medic responsible the encounter. |
| `encounterClass`              | String | The class of the encounter, such as `ambulatory`, `emergency`, `inpatient`, `wellness`, or `urgentcare` |
| `code`              | String | Encounter code from SNOMED-CT |
| `description`              | String | Description of the type of encounter. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT, only if this encounter targeted a specific condition. |
| `reasonDescription`              | String | Description of the reason code. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/encounter?Id"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "id": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "start": "2010-01-23T17:45:28Z",
    "stop": "2010-01-23T18:10:28Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "participantactor": "1dfa9e64-9af1-497d-be21-52be529fdbc3",
    "encounterClass": "ambulatory",
    "code": "185345009",
    "description": "Encounter for symptom",
    "reasonCode": "10509002",
    "reasonDescription": "Acute bronchitis"
  }
]
```

---

### Get Encounter by patient ID

* [Return](#sections)

> Version 4.2.1

Returns encounter informations by ID

```plaintext
GET /encounter/{patient_id}
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `patient_id`              | UUID | Yes | Primary Key. Unique Identifier of the patient. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the encounter. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter started. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter concluded. |
| `patient`              | UUID | Foreign key to the Patient. |
| `participantactor`    | UUID | ID of the medic responsible the encounter. |
| `encounterClass`              | String | The class of the encounter, such as `ambulatory`, `emergency`, `inpatient`, `wellness`, or `urgentcare` |
| `code`              | String | Encounter code from SNOMED-CT |
| `description`              | String | Description of the type of encounter. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT, only if this encounter targeted a specific condition. |
| `reasonDescription`              | String | Description of the reason code. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/encounter?Id"
```

* Response:

```json
[
  {
    "_id": "63741993851aada93ab6f3cc",
    "id": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "start": "2010-01-23T17:45:28Z",
    "stop": "2010-01-23T18:10:28Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "participantActor": "1dfa9e64-9af1-497d-be21-52be529fdbc3",
    "encounterClass": "ambulatory",
    "code": "185345009",
    "description": "regular check-up",
    "reasonCode": "10509002",
    "reasonDescription": "Acute bronchitis"
  }
]
```

---

### Post Encounter

* [Return](#sections)

> Version 4.2.1

Posts Encounter data into the database

```plaintext
POST /encounter
```

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Primary Key. Unique Identifier of the encounter. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter started. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter concluded. |
| `patient`              | UUID | Foreign key to the Patient. |
| `participantactor`    | UUID | ID of the medic responsible the encounter. |
| `encounterClass`              | String | The class of the encounter, such as `ambulatory`, `emergency`, `inpatient`, `wellness`, or `urgentcare` |
| `code`              | String | Encounter code from SNOMED-CT |
| `description`              | String | Description of the type of encounter. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT, only if this encounter targeted a specific condition. |
| `reasonDescription`              | String | Description of the reason code. |

```json
[
  {
  "encounter": {
    "id": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "start": "2010-01-23T17:45:28Z",
    "stop": "2010-01-23T18:10:28Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "participantactor": "1dfa9e64-9af1-497d-be21-52be529fdbc3",
    "encounterClass": "ambulatory",
    "code": "185345009",
    "description": "Encounter for symptom",
    "reasonCode": "10509002",
    "reasonDescription": "Acute bronchitis"
    }
  }
]
```

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X POST -H "PRIVATE-TOKEN: <your_access_token>" -d @path/to/file.json "https://gitlab.example.com/api/suco/encounter"
```

---

### Put Encounter

* [Return](#sections)

> Version 4.2.1

Updates encounter with the request body passed by the user

```plaintext
PUT /encounter/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the encounter. |

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the encounter. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter started. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the encounter concluded. |
| `patient`              | UUID | Foreign key to the Patient. |
| `participantactor`    | UUID | ID of the medic responsible the encounter. |
| `encounterClass`              | String | The class of the encounter, such as `ambulatory`, `emergency`, `inpatient`, `wellness`, or `urgentcare` |
| `code`              | String | Encounter code from SNOMED-CT |
| `description`              | String | Description of the type of encounter. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT, only if this encounter targeted a specific condition. |
| `reasonDescription`              | String | Description of the reason code. |

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "id": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "start": "2010-01-23T17:45:28Z",
    "stop": "2010-01-23T18:10:28Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "participantactor": "1dfa9e64-9af1-497d-be21-52be529fdbc3",
    "encounterClass": "ambulatory",
    "code": "185345009",
    "description": "Encounter for symptom",
    "reasonCode": "10509002",
    "reasonDescription": "Acute bronchitis"
  }
]
```

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X PUT -H "PRIVATE-TOKEN: <your_access_token>" -d @json_file "https://gitlab.example.com/api/suco/encounter?id"
```

---

### Delete Encounter

* [Return](#sections)

> Version 4.2.1

Updates encounter with the request body passed by the user

```plaintext
DELETE /encounter/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the encounter. |

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X DELETE -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/encounter?id"
```

---

### List all Diagnostic Reports

* [Return](#sections)

> Version 4.2.1

Returns all diagnostic reports

```plaintext
GET /diagnostic/
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `pageNumber`  | integer | No | Number of the page you need. |
| `nDiagnostic`  | integer | No | Number of objects per page. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the Diagnostic Report |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID |  Foreign key to the Encounter.  |
| `status`              | String |  The status of the diagnostic report.  |
| `resultsInterpreter`              | UUID |The practitioner's ID that is responsible for the report's conclusions and interpretations. |
| `result`              | List | 	A list of observations that are part of this diagnostic report. |
| `result.observation.category`              | String | Observation category. |
| `result.observation.code`              | String | 	Describes what was observed via LOINC Code. |
| `result.observation.value`              | Int | 	Results of the observation linked to the Code. |
| `result.observation.units`              | String | The units of measure for the value. |
| `result.observation.type`              | String | The datatype of `Value`: `text` or `numeric` |
| `study`              | List | A list of UUIDs from images associated with this report. The images are generally created during the diagnostic process, and may be directly of the patient, or of treated specimens (i.e. slides of interest). |
| `conclusionCode`              | List | 	One or more codes that represent the summary conclusion (interpretation/impression) of the diagnostic report. Uses SNOMED CT Clinical Findings codes. |
| `reliability`              | List | List of floats that represents the confidence percentages of the Conclusion codes|

* All possible Status of a Diagnostic Report:

| Code | Definition |
|:--------|:----------------------------------------------------|
| registered |The existence of the report is registered, but there is nothing yet available. |
| partial | This is a partial (e.g. initial, interim or preliminary) report: data in the report may be incomplete or unverified. |
| preliminary | Verified early results are available, but not all results are final. |
| final | The report is complete and verified by an authorized person. |
| amended | Subsequent to being final, the report has been modified. This includes any change in the results, diagnosis, narrative text, or other content of a report that has been issued. |
| corrected | Subsequent to being final, the report has been modified to correct an error in the report or referenced results. |
| appended | Subsequent to being final, the report has been modified by adding new content. The existing content is unchanged. |
| cancelled | The report is unavailable because the measurement was not started or not completed (also sometimes called "aborted"). |
| entered-in-error	| The report has been withdrawn following a previous final release. This electronic record should never have existed, though it is possible that real-world decisions were based on it. (If real-world activity has occurred, the status should be "cancelled" rather than "entered-in-error".). |
| unknown | The authoring/source system does not know which of the status values currently applies for this observation. Note: This concept is not to be used for "other" - one of the listed statuses is presumed to apply, but the authoring/source system does not know which. |

* All possible categories of a Observation:

|Code	          | Display | Definition |
|:--------------|:--------|:-----------|
| social-history|	Social History |	Social History Observations define the patient's occupational, personal (e.g., lifestyle), social, familial, and environmental history and health risk factors that may impact the patient's health. |
| vital-signs |	Vital Signs |	Clinical observations measure the body's basic functions such as blood pressure, heart rate, respiratory rate, height, weight, body mass index, head circumference, pulse oximetry, temperature, and body surface area. |
| imaging |	Imaging |	Observations generated by imaging. The scope includes observations regarding plain x-ray, ultrasound, CT, MRI, angiography, echocardiography, and nuclear medicine. |
| laboratory |Laboratory |	The results of observations generated by laboratories. Laboratory results are typically generated by laboratories providing analytic services in areas such as chemistry, hematology, serology, histology, cytology, anatomic pathology (including digital pathology), microbiology, and/or virology. These observations are based on analysis of specimens obtained from the patient and submitted to the laboratory. |
| procedure	| Procedure |	Observations generated by other procedures. This category includes observations resulting from interventional and non-interventional procedures excluding laboratory and imaging (e.g., cardiology catheterization, endoscopy, electrodiagnostics, etc.). Procedure results are typically generated by a clinician to provide more granular information about component observations made during a procedure. An example would be when a gastroenterologist reports the size of a polyp observed during a colonoscopy. |
| survey | Survey |	Assessment tool/survey instrument observations (e.g., Apgar Scores, Montreal Cognitive Assessment (MoCA)). |
| exam	| Exam	| Observations generated by physical exam findings including direct observations made by a clinician and use of simple instruments and the result of simple maneuvers performed directly on the patient's body. |
| therapy	| Therapy	| Observations generated by non-interventional treatment protocols (e.g. occupational, physical, radiation, nutritional and medication therapy) |
|activity |	Activity |	Observations that measure or record any bodily activity that enhances or maintains physical fitness and overall health and wellness. Not under direct supervision of practitioner such as a physical therapist. (e.g., laps swum, steps, sleep data) |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/diagnostic"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "id": "42bfe9b8-3f5a-11ed-b878-0242ac120002",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "status": "final",
    "resultsInterpreter": "cf546bba-6fa5-430f-9656-300dda3cf865",
    "result":[
      {   "category": "laboratory",
          "code": "718-7",
          "value": "15.1",
          "units": "g/dL",
          "type": "numeric"}
    ],
    "study":[
      "1d88a815-fbc2-9b61-b582-944100e41fd1"
    ],
    "conclusionCode": [
      2694001
    ],
    "reliability": [
      None
    ]
  }, ...
]
```
---

### Get Diagnostic Reports

* [Return](#sections)

> Version 4.2.1

Returns all diagnostic reports based on a patient/encounter ID or its own ID

```plaintext
GET /diagnostic/{id}
GET /diagnostic/patient/{patient_id}
GET /diagnostic/encounter/{encounter_id}
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the patient. |
| `patient_id`  | UUID | Yes | Unique Identifier of the Patient. |
| `encounter_id`  | UUID | Yes | Unique Identifier of the Encounter. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the Diagnostic Report |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID |  Foreign key to the Encounter.  |
| `status`              | String |  The status of the diagnostic report.  |
| `resultsInterpreter`              | UUID |The practitioner's ID that is responsible for the report's conclusions and interpretations. |
| `result`              | List | 	A list of observations that are part of this diagnostic report. |
| `result.observation.category`              | String | Observation category. |
| `result.observation.code`              | String | 	Describes what was observed via LOINC Code. |
| `result.observation.value`              | Int | 	Results of the observation linked to the Code. |
| `result.observation.units`              | String | The units of measure for the value. |
| `result.observation.type`              | String | The datatype of `Value`: `text` or `numeric` |
| `study`              | List | A list of UUIDs from images associated with this report. The images are generally created during the diagnostic process, and may be directly of the patient, or of treated specimens (i.e. slides of interest). |
| `conclusionCode`              | List | 	One or more codes that represent the summary conclusion (interpretation/impression) of the diagnostic report. Uses SNOMED CT Clinical Findings codes. |
| `reliability`              | List | List of floats that represents the confidence percentages of the Conclusion codes|

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/diagnostic"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "id": "42bfe9b8-3f5a-11ed-b878-0242ac120002",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "status": "final",
    "resultsInterpreter": "cf546bba-6fa5-430f-9656-300dda3cf865",
    "result":[
      {   "category": "laboratory",
          "code": "718-7",
          "value": "15.1",
          "units": "g/dL",
          "type": "numeric"}
    ],
    "study":[
      "1d88a815-fbc2-9b61-b582-944100e41fd1"
    ],
    "conclusionCode": [
      2694001
    ],
    "reliability": [
      None
    ]
  }
]
```
---

### Post Diagnostic Report

* [Return](#sections)

> Version 2.2.1

Posts Diagnostic Report into the database

```plaintext
POST /diagnostic
```
* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Primary Key. Unique Identifier of the Diagnostic Report |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID |  Foreign key to the Encounter.  |
| `status`              | String |  The status of the diagnostic report.  |
| `resultsInterpreter`              | UUID |The practitioner's ID that is responsible for the report's conclusions and interpretations. |
| `result`              | List | 	A list of observations that are part of this diagnostic report. |
| `result.observation.category`              | String | Observation category. |
| `result.observation.code`              | String | 	Describes what was observed via LOINC Code. |
| `result.observation.value`              | Int | 	Results of the observation linked to the Code. |
| `result.observation.units`              | String | The units of measure for the value. |
| `result.observation.type`              | String | The datatype of `Value`: `text` or `numeric` |
| `study`              | List | A list of UUIDs from images associated with this report. The images are generally created during the diagnostic process, and may be directly of the patient, or of treated specimens (i.e. slides of interest). |
| `conclusionCode`              | List | 	One or more codes that represent the summary conclusion (interpretation/impression) of the diagnostic report. Uses SNOMED CT Clinical Findings codes. |

```json
[
  {
    "id": "42bfe9b8-3f5a-11ed-b878-0242ac120002",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "status": "final",
    "resultsInterpreter": "cf546bba-6fa5-430f-9656-300dda3cf865",
    "result":[
      {   "category": "laboratory",
          "code": "718-7",
          "value": "15.1",
          "units": "g/dL",
          "type": "numeric"}
    ],
    "study":[
      "1d88a815-fbc2-9b61-b582-944100e41fd1"
    ]
    "conclusionCode": [
      2694001
    ]
  }
]
```

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X POST -H "PRIVATE-TOKEN: <your_access_token>" -d @path/to/file.json "https://gitlab.example.com/api/suco/diagnostic"
```
---

### Put Diagnostic Report

* [Return](#sections)

> Version 4.2.1

Updates diagnostic report with the request body passed by the user

```plaintext
PUT /diagnostic/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the diagnostic report. |

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Primary Key. Unique Identifier of the Diagnostic Report |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID |  Foreign key to the Encounter.  |
| `status`              | String |  The status of the diagnostic report.  |
| `resultsInterpreter`              | UUID |The practitioner's ID that is responsible for the report's conclusions and interpretations. |
| `result`              | List | 	A list of observations that are part of this diagnostic report. |
| `result.observation.category`              | String | Observation category. |
| `result.observation.code`              | String | 	Describes what was observed via LOINC Code. |
| `result.observation.value`              | Int | 	Results of the observation linked to the Code. |
| `result.observation.units`              | String | The units of measure for the value. |
| `result.observation.type`              | String | The datatype of `Value`: `text` or `numeric` |
| `study`              | List | A list of UUIDs from images associated with this report. The images are generally created during the diagnostic process, and may be directly of the patient, or of treated specimens (i.e. slides of interest). |
| `conclusionCode`              | List | 	One or more codes that represent the summary conclusion (interpretation/impression) of the diagnostic report. Uses SNOMED CT Clinical Findings codes. |

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "id": "42bfe9b8-3f5a-11ed-b878-0242ac120002",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "status": "final",
    "resultsInterpreter": "cf546bba-6fa5-430f-9656-300dda3cf865",
    "result":[
      {   "category": "laboratory",
          "code": "718-7",
          "value": "15.1",
          "units": "g/dL",
          "type": "numeric"}
    ],
    "study":[
      "1d88a815-fbc2-9b61-b582-944100e41fd1"
    ]
    "conclusionCode": [
      2694001
    ]
  }
]
```

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X PUT -H "PRIVATE-TOKEN: <your_access_token>" -d @json_file "https://gitlab.example.com/api/suco/diagnostic?id"
```

---

### Delete Diagnostic Report

* [Return](#sections)

> Version 4.2.1

Deletes diagnostic report by ID.

```plaintext
DELETE /diagnostic/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the diagnostic report. |

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X DELETE -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/diagnostic?id"
```

---
### List all Images

* [Return](#sections)

> Version 4.2.1

Returns all images separated in pages.

```plaintext
GET /images/
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `pageNumber`  | integer | No | Number of the page you need. |
| `nEncounter`  | integer | No | Number of patients per page. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Unique identifier of the image. |
| `date`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the image was taken. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the image was taken. |
| `imagePath`              | String | Path to image in the database. |
| `boxesInfo.label`              | String | |
| `boxesInfo.confidence`              | String | |
| `boxesInfo.topleft.x`              | String | Top left x coordinates. |
| `boxesInfo.topleft.y`              | String | Top left y coordinates |
| `boxesInfo.bottomright.x`              | String | Bottom right x coordinates. |
| `boxesInfo.bottomright.y`              | String | Bottom right y coordinates. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/images/"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
    "date": "2003-11-01T20:30:34Z",
    "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
    "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
    "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png"
    "boxesInfo":{   
      "label": "Platelets",
      "confidence": "0.24365237425374",
      "topleft":{
        "x": "0",
        "y": "0"
      },
      "bottomright": {
        "x": "372",
        "y": "408"
      },
    }  
  }
]
```

---

### Get Images

* [Return](#sections)

> Version 4.2.1

Returns all imaging studies by ID, patient_id or by encounter_id.

```plaintext
GET /images/{Id}
GET /images/encounter/{encounter_id}
GET /images/patient/{patient}
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`  | UUID | No | Primary Key. Unique Identifier of the ImagingStudies. |
| `encounter`              | UUID | No | Foreign key to the Encounter. |
| `patient`               | UUID | No | Foreign key to the Patient. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Unique identifier of the image. |
| `date`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the image was taken. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the image was taken. |
| `imagePath`              | String | Path to image in the database. |
| `boxesInfo.label`              | String | |
| `boxesInfo.confidence`              | String | |
| `boxesInfo.topleft.x`              | String | Top left x coordinates. |
| `boxesInfo.topleft.y`              | String | Top left y coordinates |
| `boxesInfo.bottomright.x`              | String | Bottom right x coordinates. |
| `boxesInfo.bottomright.y`              | String | Bottom right y coordinates. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/images?id"
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/images?encounter"
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/images?patient"
```

* Response:

```json
[
  {
      "_id": "6386cb3e921e2b2ff211c25a",
      "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
      "date": "2003-11-01T20:30:34Z",
      "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
      "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
      "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png"
      "boxesInfo":{   
        "label": "Platelets",
        "confidence": "0.24365237425374",
        "topleft":{
          "x": "0",
          "y": "0"
        },
        "bottomright": {
          "x": "372",
          "y": "408"
        },
      }  
  }
]
```

---

### Post Images

* [Return](#sections)

> Version 2.2.1

Post Images data into the database

```plaintext
POST /image
```

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `id`              | UUID | Unique identifier of the image. |
| `date`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the image was taken. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the image was taken. |
| `imagePath`              | String | Path to image in the database. |
| `boxesInfo.label`              | String | |
| `boxesInfo.confidence`              | String | |
| `boxesInfo.topleft.x`              | String | Top left x coordinates. |
| `boxesInfo.topleft.y`              | String | Top left y coordinates |
| `boxesInfo.bottomright.x`              | String | Bottom right x coordinates. |
| `boxesInfo.bottomright.y`              | String | Bottom right y coordinates. |

```json
[
  {
    "image": {
    "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
    "date": "2003-11-01T20:30:34Z",
    "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
    "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
    "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png"
    "boxesInfo":{   
        "label": "Platelets",
        "confidence": "0.28888232721",
        "topleft":{
          "x": "0",
          "y": "0"
        },
        "bottomright": {
          "x": "372",
          "y": "408"
        },
      }  
    }
  }  
]
```

If successful, returns [`200`](../../api/index.md#status-codes)

* Request:

```shell
curl -X POST -H "PRIVATE-TOKEN: <your_access_token>" -d @path/to/file.json "https://gitlab.example.com/api/suco/images"
```
---

### Put Image

* [Return](#sections)

> Version 4.2.1

Updates image with the request body passed by the user

```plaintext
PUT /image/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the image. |

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID, is equal to the `id` field. |
| `id`              | UUID | Unique identifier of the image. |
| `date`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the image was taken. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the image was taken. |
| `imagePath`              | String | Path to image in the database. |
| `boxesInfo.label`              | String | |
| `boxesInfo.confidence`              | String | |
| `boxesInfo.topleft.x`              | String | Top left x coordinates. |
| `boxesInfo.topleft.y`              | String | Top left y coordinates |
| `boxesInfo.bottomright.x`              | String | Bottom right x coordinates. |
| `boxesInfo.bottomright.y`              | String | Bottom right y coordinates. |

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "id": "1d88a815-fbc2-9b61-b582-944100e41fd1",
    "date": "2003-11-01T20:30:34Z",
    "patient": "ada32eac-0c43-38d0-14e5-e2e545631eef",
    "encounter": "13efcb79-c259-e4a5-2258-ea655ceeeb48",
    "imagePath": "images/blood-cell/1d88a815-fbc2-9b61-b582-944100e41fd1.png"
    "boxesInfo":{   
        "label": "Platelets",
        "confidence": "0.28888232721",
        "topleft":{
          "x": "0",
          "y": "0"
        },
        "bottomright": {
          "x": "372",
          "y": "408"
        },
      }  
  }  
]
```

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X PUT -H "PRIVATE-TOKEN: <your_access_token>" -d @json_file "https://gitlab.example.com/api/suco/image?id"
```

---

### Delete Image

* [Return](#sections)

> Version 4.2.1

Deletes image by ID

```plaintext
DELETE /image/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the image. |

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X DELETE -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/image?id"
```

---

### List all Medications

* [Return](#sections)

> Version 4.2.1

Returns all medications separated in pages.

```plaintext
GET /medication/
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `pageNumber`  | integer | No | Number of the page you need. |
| `nEncounter`  | integer | No | Number of patients per page. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the medication was prescribed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the prescription ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the medication was prescribed. |
| `code`              | String | Medication code from RxNorm. |
| `description`              | String | Description of the medication. |
| `dispenses`              | Numeric | The number of times the prescription was filled. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this medication was prescribed. |
| `reasonDescription`              | String | 
Description of the reason code. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/medications"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "start": "2020-11-25T12:12:33Z",
    "stop": "2020-12-30T21:12:49Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "562251",
    "description": "Amoxicillin 250 MG / Clavulanate 125 MG Oral Tablet",
    "dispenses": "1",
    "reasonCode": "36971009",
    "reasonDescription": "Sinusitis (disorder)"
  }, ...
]
```

---

## Get Medications

* [Return](#sections)

> Version 4.2.1

Medication data by patient or by encounter.

```plaintext
GET /medications/encounter/{encounter_id}
GET /medications/patient/{patient_id}
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `encounter_id`              | UUID | No | Foreign key to the Encounter. |
| `patient_id`               | UUID | No | Foreign key to the Patient. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the medication was prescribed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the prescription ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the medication was prescribed. |
| `code`              | String | Medication code from RxNorm. |
| `description`              | String | Description of the medication. |
| `dispenses`              | Numeric | The number of times the prescription was filled. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this medication was prescribed. |
| `reasonDescription`              | String | 
Description of the reason code. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/medications"
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/medications?encounter"
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/medications?patient"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "start": "2020-11-25T12:12:33Z",
    "stop": "2020-12-30T21:12:49Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "562251",
    "description": "Amoxicillin 250 MG / Clavulanate 125 MG Oral Tablet",
    "dispenses": "1",
    "reasonCode": "36971009",
    "reasonDescription": "Sinusitis (disorder)"
  }
]
```

---

### Post Medications

* [Return](#sections)

> Version 2.2.1

Post Medication data into the database

```plaintext
POST /medication
```

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the medication was prescribed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the prescription ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the medication was prescribed. |
| `code`              | String | Medication code from RxNorm. |
| `description`              | String | Description of the medication. |
| `dispenses`              | Numeric | The number of times the prescription was filled. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this medication was prescribed. |
| `reasonDescription`              | String | 
Description of the reason code. |

```json
[
  {
    "start": "2020-11-25T12:12:33Z",
    "stop": "2020-12-30T21:12:49Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "562251",
    "description": "Amoxicillin 250 MG / Clavulanate 125 MG Oral Tablet",
    "dispenses": "1",
    "reasonCode": "36971009",
    "reasonDescription": "Sinusitis (disorder)"
  }
]
```

If successful, returns [`200`](../../api/index.md#status-codes)

* Request:

```shell
curl -X POST -H "PRIVATE-TOKEN: <your_access_token>" -d @path/to/file.json "https://gitlab.example.com/api/suco/medications"
```

---

### Put Medication

* [Return](#sections)

> Version 4.2.1

Updates medication with the request body passed by the user

```plaintext
PUT /medication/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the medication. |

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the medication was prescribed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the prescription ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the medication was prescribed. |
| `code`              | String | Medication code from RxNorm. |
| `description`              | String | Description of the medication. |
| `dispenses`              | Numeric | The number of times the prescription was filled. |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this medication was prescribed. |
| `reasonDescription`              | String | 
Description of the reason code. |

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "start": "2020-11-25T12:12:33Z",
    "stop": "2020-12-30T21:12:49Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "562251",
    "description": "Amoxicillin 250 MG / Clavulanate 125 MG Oral Tablet",
    "dispenses": "1",
    "reasonCode": "36971009",
    "reasonDescription": "Sinusitis (disorder)"
  }
]

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X PUT -H "PRIVATE-TOKEN: <your_access_token>" -d @json_file "https://gitlab.example.com/api/suco/medication?id"
```

---

### Delete Medication

* [Return](#sections)

> Version 4.2.1

Delete medication by ID.

```plaintext
DELETE /medication/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the medication. |

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X DELETE -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/medication?id"
```

---

### List all Procedures

 [Return](#sections)

> Version 4.2.1

Returns all procedures separated in pages.

```plaintext
GET /procedure/
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `pageNumber`  | integer | No | Number of the page you need. |
| `nEncounter`  | integer | No | Number of patients per page. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the procedure was performed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the procedure was completed, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the procedure was performed. |
| `code`              | String | Procedure code from SNOMED-CT |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this procedure was performed. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/procedures?"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "start": "2020-11-25T21:12:49Z",
    "stop": "2020-11-25T21:23:25Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "261352009",
    "reasonCode": "840544004",
  },...
]
```


## Get Procedures

* [Return](#sections)

> Version 2.1.0.

Procedures data by patient or by encounter.

```plaintext
GET /procedures/encounter/{encounter_id}
GET /procedures/patient/{patient_id}
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `encounter`              | UUID | No | Foreign key to the Encounter. |
| `patient`               | UUID | No | Foreign key to the Patient. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the procedure was performed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the procedure was completed, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the procedure was performed. |
| `code`              | String | Procedure code from SNOMED-CT |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this procedure was performed. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/procedures?encounter"
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/procedures?patient"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "start": "2020-11-25T21:12:49Z",
    "stop": "2020-11-25T21:23:25Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "261352009",
    "reasonCode": "840544004",
  }  
]
```

---

## Post Procedures

* [Return](#sections)

> Version 2.2.1

Post Procedures data into the database

```plaintext
POST /procedures
```

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the procedure was performed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the procedure was completed, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the procedure was performed. |
| `code`              | String | Procedure code from SNOMED-CT |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this procedure was performed. |

```json
[
  {
    "start": "2020-11-25T21:12:49Z",
    "stop": "2020-11-25T21:23:25Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "261352009",
    "reasonCode": "840544004",
  }  
]
```

If successful, returns [`200`](../../api/index.md#status-codes)

* Request:

```shell
curl -X POST -H "PRIVATE-TOKEN: <your_access_token>" -d @path/to/file.json "https://gitlab.example.com/api/suco/procedure"
```
---

### Put Procedure

* [Return](#sections)

> Version 4.2.1

Updates procedure with the request body passed by the user

```plaintext
PUT /procedure/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the encounter. |

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID. |
| `start`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'`) | The date and time the procedure was performed. |
| `stop`              | iso8601 UTC Date (`yyyy-MM-dd'T'HH:mm'Z'` | The date and time the procedure was completed, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter where the procedure was performed. |
| `code`              | String | Procedure code from SNOMED-CT |
| `reasonCode`              | String | Diagnosis code from SNOMED-CT specifying why this procedure was performed. |

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "start": "2020-11-25T21:12:49Z",
    "stop": "2020-11-25T21:23:25Z",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "261352009",
    "reasonCode": "840544004",
  }  
]
```

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X PUT -H "PRIVATE-TOKEN: <your_access_token>" -d @json_file "https://gitlab.example.com/api/suco/procedure?id"
```

---

### Delete Procedure

* [Return](#sections)

> Version 4.2.1

Delete procedure by ID.

```plaintext
DELETE /procedure/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the procedure. |

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X DELETE -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/procedure?id"
```

---

### List all allergies

* [Return](#sections)

> Version 3.0.1.

Returns all allergies separated in pages.

```plaintext
GET /allergy/
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `pageNumber`  | integer | No | Number of the page you need. |
| `nEncounter`  | integer | No | Number of patients per page. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID. |
| `start`              | Date (`YYYY-MM-DD`) | The date the allergy was diagnosed. |
| `stop`              | Date (`YYYY-MM-DD`) | The date the allergy ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter when the allergy was diagnosed. |
| `code`              | String | Allergy code |
| `system`              | String | Terminology system of the Allergy code. `RxNorm` if this is a medication allergy, otherwise `SNOMED-CT`. |
| `description`              | String | Description of the `Allergy` |
| `type`              | String | Identify entry as an `allergy` or `intolerance`. |
| `category`              | String | Identify the category as `drug`, `medication`, `food`, or `environment`. |
| `reaction1`              | String | Optional SNOMED code of the patients reaction. |
| `description1`              | String | Optional description of the `Reaction1` SNOMED code. |
| `severity1`              | String | Severity of the reaction: `MILD`, `MODERATE`, or `SEVERE`. |
| `reaction2`              | String | Optional SNOMED code of the patients second reaction. |
| `description2`              | String | Optional description of the `Reaction2` SNOMED code. |
| `severity2`              | String | Severity of the second reaction: `MILD`, `MODERATE`, or `SEVERE`. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/allergies?encounter"
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/allergies?patient"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "start": "2010-03-17",
    "stop": "",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "762952008",
    "system": "Unknown",
    "description": "Peanut (substance)",
    "type": "allergy",
    "category": "food",
    "reaction1": "247472004",
    "description1": "Wheal (finding)",
    "severity1": "MODERATE",
    "reaction2": "271807003",
    "description2": "Eruption of skin (disorder)",
    "severity2": "MILD",
    },...
]
```


---

## Get Allergies

* [Return](#sections)

> Version 3.0.1.

Get patient allergy data.

```plaintext
GET /allergy/patient/{patient_id}
GET /allergy/encounter/{encounter_id}
```
* Supported attributes:

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `patient_id`               | UUID | No | Foreign key to the Patient. |
| `encounter_id`               | UUID | No | Foreign key to the Encounter. |

If successful, returns [`200`](../../api/index.md#status-codes) and the following
response attributes:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID. |
| `start`              | Date (`YYYY-MM-DD`) | The date the allergy was diagnosed. |
| `stop`              | Date (`YYYY-MM-DD`) | The date the allergy ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter when the allergy was diagnosed. |
| `code`              | String | Allergy code |
| `system`              | String | Terminology system of the Allergy code. `RxNorm` if this is a medication allergy, otherwise `SNOMED-CT`. |
| `description`              | String | Description of the `Allergy` |
| `type`              | String | Identify entry as an `allergy` or `intolerance`. |
| `category`              | String | Identify the category as `drug`, `medication`, `food`, or `environment`. |
| `reaction1`              | String | Optional SNOMED code of the patients reaction. |
| `description1`              | String | Optional description of the `Reaction1` SNOMED code. |
| `severity1`              | String | Severity of the reaction: `MILD`, `MODERATE`, or `SEVERE`. |
| `reaction2`              | String | Optional SNOMED code of the patients second reaction. |
| `description2`              | String | Optional description of the `Reaction2` SNOMED code. |
| `severity2`              | String | Severity of the second reaction: `MILD`, `MODERATE`, or `SEVERE`. |

* Request:

```shell
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/allergies?encounter"
curl -X GET -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/allergies?patient"
```

* Response:

```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "start": "2010-03-17",
    "stop": "",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "762952008",
    "system": "Unknown",
    "description": "Peanut (substance)",
    "type": "allergy",
    "category": "food",
    "reaction1": "247472004",
    "description1": "Wheal (finding)",
    "severity1": "MODERATE",
    "reaction2": "271807003",
    "description2": "Eruption of skin (disorder)",
    "severity2": "MILD",
  }
]
```

---

## Post Allergies

* [Return](#sections)

> Version 3.0.1.

Post patient allergy data.

```plaintext
POST /allergy
```

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `start`              | Date (`YYYY-MM-DD`) | The date the allergy was diagnosed. |
| `stop`              | Date (`YYYY-MM-DD`) | The date the allergy ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter when the allergy was diagnosed. |
| `code`              | String | Allergy code |
| `system`              | String | Terminology system of the Allergy code. `RxNorm` if this is a medication allergy, otherwise `SNOMED-CT`. |
| `description`              | String | Description of the `Allergy` |
| `type`              | String | Identify entry as an `allergy` or `intolerance`. |
| `category`              | String | Identify the category as `drug`, `medication`, `food`, or `environment`. |
| `reaction1`              | String | Optional SNOMED code of the patients reaction. |
| `description1`              | String | Optional description of the `Reaction1` SNOMED code. |
| `severity1`              | String | Severity of the reaction: `MILD`, `MODERATE`, or `SEVERE`. |
| `reaction2`              | String | Optional SNOMED code of the patients second reaction. |
| `description2`              | String | Optional description of the `Reaction2` SNOMED code. |
| `severity2`              | String | Severity of the second reaction: `MILD`, `MODERATE`, or `SEVERE`. |


```json
[
  {
    "start": "2010-03-17",
    "stop": "",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "762952008",
    "system": "Unknown",
    "description": "Peanut (substance)",
    "type": "allergy",
    "category": "food",
    "reaction1": "247472004",
    "description1": "Wheal (finding)",
    "severity1": "MODERATE",
    "reaction2": "271807003",
    "description2": "Eruption of skin (disorder)",
    "severity2": "MILD",
  }
]
```

If successful, returns [`200`](../../api/index.md#status-codes)

* Request:

```shell
curl -X POST -H "PRIVATE-TOKEN: <your_access_token>" -d @path/to/file.json "https://gitlab.example.com/api/suco/allergies"
```

---

### Put Allergy

* [Return](#sections)

> Version 4.2.1

Updates allergies with the request body passed by the user

```plaintext
PUT /allergy/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the allergy. |

* Request body:

| Attribute                | Type     | Description           |
|:-------------------------|:---------|:----------------------|
| `_id`              | UUID | mongoDB ID. |
| `start`              | Date (`YYYY-MM-DD`) | The date the allergy was diagnosed. |
| `stop`              | Date (`YYYY-MM-DD`) | The date the allergy ended, if applicable. |
| `patient`              | UUID | Foreign key to the Patient. |
| `encounter`              | UUID | Foreign key to the Encounter when the allergy was diagnosed. |
| `code`              | String | Allergy code |
| `system`              | String | Terminology system of the Allergy code. `RxNorm` if this is a medication allergy, otherwise `SNOMED-CT`. |
| `description`              | String | Description of the `Allergy` |
| `type`              | String | Identify entry as an `allergy` or `intolerance`. |
| `category`              | String | Identify the category as `drug`, `medication`, `food`, or `environment`. |
| `reaction1`              | String | Optional SNOMED code of the patients reaction. |
| `description1`              | String | Optional description of the `Reaction1` SNOMED code. |
| `severity1`              | String | Severity of the reaction: `MILD`, `MODERATE`, or `SEVERE`. |
| `reaction2`              | String | Optional SNOMED code of the patients second reaction. |
| `description2`              | String | Optional description of the `Reaction2` SNOMED code. |
| `severity2`              | String | Severity of the second reaction: `MILD`, `MODERATE`, or `SEVERE`. |


```json
[
  {
    "_id": "6386cb3e921e2b2ff211c25a",
    "start": "2010-03-17",
    "stop": "",
    "patient": "1d604da9-9a81-4ba9-80c2-de3375d59b40",
    "encounter": "d0c40d10-8d87-447e-836e-99d26ad52ea5",
    "code": "762952008",
    "system": "Unknown",
    "description": "Peanut (substance)",
    "type": "allergy",
    "category": "food",
    "reaction1": "247472004",
    "description1": "Wheal (finding)",
    "severity1": "MODERATE",
    "reaction2": "271807003",
    "description2": "Eruption of skin (disorder)",
    "severity2": "MILD",
  }
]
```

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X PUT -H "PRIVATE-TOKEN: <your_access_token>" -d @json_file "https://gitlab.example.com/api/suco/allergy?id"
```

---

### Delete Allergy

* [Return](#sections)

> Version 4.2.1

Delete allergy by ID.

```plaintext
DELETE /allergy/{id}
```

| Attribute                | Type     | Required | Description           |
|:-------------------------|:---------|:---------|:----------------------|
| `id`              | UUID | Yes | Primary Key. Unique Identifier of the allergy. |

If successful, returns [`200`](../../api/index.md#status-codes).

* Request:

```shell
curl -X DELETE -H "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/suco/allergy?id"
```

---
