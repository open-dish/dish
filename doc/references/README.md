# Artificial Intelligence and Hematology

<div class="csl-entry">Radakovich, N., Nagy, M., &#38; Nazha, A. (2020). Artificial Intelligence in Hematology: Current Challenges and Opportunities. <i>Current Hematologic Malignancy Reports</i>, <i>15</i>(3), 203–210. [Online](https://link.springer.com/article/10.1007/s11899-020-00575-4)</div>

# Analysis of red blood cells from peripheral blood smear images for anemia detection: a methodological review
TY  - JOUR
AU  - K.T., Navya
AU  - Prasad, Keerthana
AU  - Singh, Brij Mohan Kumar
PY  - 2022
DA  - 2022/09/01
TI  - Analysis of red blood cells from peripheral blood smear images for anemia detection: a methodological review
JO  - Medical & Biological Engineering & Computing
SP  - 2445
EP  - 2462
VL  - 60
IS  - 9

AB  - Anemia is a blood disorder which is caused due to inadequate red blood cells and hemoglobin concentration. It occurs in all phases of life cycle but is more dominant in pregnant women and infants. According to the survey conducted by the World Health Organization (WHO) (McLean et al., Public Health Nutr 12(4):444–454, 2009), anemia affects 1.62 billion people constituting 24.8% of the population and is considered the world’s second leading cause of illness. The Peripheral Blood Smear (PBS) examination plays an important role in evaluating hematological disorders. Anemia is diagnosed using PBS. Being the most powerful analytical tool, manual analysis approach is still in use even though it is tedious, prone to errors, time-consuming and requires qualified laboratorians. It is evident that there is a need for an inexpensive, automatic and robust technique to detect RBC disorders from PBS. Automation of PBS analysis is very active field of research that motivated many research groups to develop methods using image processing. In this paper, we present a review of the methods used to analyze the characteristics of RBC from PBS images using image processing techniques. We have categorized these methods into three groups based on approaches such as RBC segmentation, RBC classification and detection of anemia, and classification of anemia. The outcome of this review has been presented as a list of observations.
SN  - 1741-0444
UR  - https://doi.org/10.1007/s11517-022-02614-z
DO  - 10.1007/s11517-022-02614-z
ID  - K.T.2022
ER  - 

# Evaluation of Anemia
By Evan M. Braunstein , MD, PhD, Johns Hopkins University School of Medicine
Last full review/revision Jul 2022| Content last modified Sep 2022

https://www.msdmanuals.com/professional/hematology-and-oncology/approach-to-the-patient-with-anemia/evaluation-of-anemia

# A review of microscopic analysis of blood cells for disease detection with AI perspective

Deshpande NM, Gite S, Aluvalu R. A review of microscopic analysis of blood cells for disease detection with AI perspective. PeerJ Comput Sci. 2021 Apr 21;7:e460. doi: 10.7717/peerj-cs.460. PMID: 33981834; PMCID: PMC8080427.
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8080427/

# Pre-trained convolutional neural networks as feature extractors toward improved malaria parasite detection in thin blood smear images
TY  - JOUR
AU  - Rajaraman, Sivaramakrishnan
AU  - Antani, Sameer
AU  - Poostchi, Mahdieh
AU  - Silamut, Kamolrat
AU  - Hossain, Md
AU  - Maude, Richard
AU  - Jaeger, Stefan
AU  - Thoma, George
PY  - 2018/04/16
SP  - 
T1  - Pre-trained convolutional neural networks as feature extractors toward improved malaria parasite detection in thin blood smear images
VL  - 6
DO  - 10.7717/peerj.4568
JO  - PeerJ
ER  - 

# Diagnostic Approach to Anemia in Adults

[UpToDate](https://www.uptodate.com/contents/diagnostic-approach-to-anemia-in-adults)

# Blood Cell Images

[Kaggle Blood Cell Images](https://www.kaggle.com/datasets/paultimothymooney/blood-cells)

[MedMNIST Images](https://medmnist.com/)


## Conversor from XPT to CSV

https://pypi.org/project/xport/

Command line:

~~~
python -m xport in.xpt out.csv
~~~
