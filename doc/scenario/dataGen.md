# Component-driven Usage Scenario for Synthea

![Composition Step 1](images/datagen/synthea-diagram.png)

### Description of the illustrated process in bullets:
* A Machine Learning service requests new patient data.
* The Adaptation Layer recieves this request and run a script calling Synthea.
* Synthea sends the generated data via the adaptation layer to the Patient Database.


