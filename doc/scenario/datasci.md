# Component-driven Usage Scenario

> Initial pipeline for the project, with some components and frameworks ideas to be considered in the future.

![Template](images/template/template.png)

> Description of the template:


1) Based on the experimental results of model training/validation, the best model is selected and uploaded to the model repository, which will be based on git and git-lfs.
2) Based on previous model metadata and the new model metadata, there will be a comparison of the metrics of the two models, and the best model will be selected.
3) The best model will be deployed to the production environment (a REST api based on docker and kubernetes), and the model will be monitored in the production environment.
4) With the model properly deployed, there will be a endpoint to make requests into it, then the client will make the request and get the response based on the data sent.

