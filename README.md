# DISH (DataSci Interoperability System for Healthcare)

Open DISH is an open source framework to provide integration and interoperability between data science services and a hospital infrastructure.

* [open-dish Data Models and APIs](doc/api)
* [Component-driven Usage Scenarios](doc/scenario)
